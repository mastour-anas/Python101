## Python Décorateur

Python a une fonctionnalité intéressante appelée décorateurs pour ajouter des fonctionnalités à un code existant.

Ceci est également appelé métaprogrammation dans le cadre du programme tente de modifier une autre partie du programme au moment de la compilation.

Essentiellement, les décorateurs fonctionnent comme des wrappers, en modifiant le comportement du code avant et après l'exécution d'une fonction cible, sans qu'il soit nécessaire de modifier la fonction elle-même, ce qui augmente la fonctionnalité d'origine et la décore.

### Ce que vous devez savoir sur les fonctions

Avant de plonger, certaines conditions préalables doivent être claires. En Python, les fonctions sont des citoyens de première classe, ce sont des objets et cela signifie que nous pouvons faire beaucoup de choses utiles avec eux.

```python
def greet(name):
    return "hello "+name

greet_someone = greet
print greet_someone("John")
# Hello John


```

Définir des fonctions dans d'autres fonctions

```python
def greet(name):
    def get_message():
        return "Hello "

    result = get_message()+name
    return result

print greet("John")

# Outputs: Hello John
```

Les fonctions peuvent être transmises comme paramètres à d'autres fonctions

```python
def greet(name):
   return "Hello " + name 

def call_func(func):
    other_name = "John"
    return func(other_name)  

print call_func(greet)

# Outputs: Hello John
```

Les fonctions peuvent renvoyer d'autres fonctions

```python
def compose_greet_func():
    def get_message():
        return "Hello there!"

    return get_message

greet = compose_greet_func()
print greet()

# Outputs: Hello there!
```

### Composition des décorateurs

Les décorateurs de fonctions sont simplement des enveloppes pour les fonctions existantes. En mettant ensemble les idées mentionnées ci-dessus, nous pouvons construire un décorateur. Dans cet exemple, considérons une fonction qui encapsule la sortie de chaîne d'une autre fonction par des balises `p` .

```python
def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)

def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

my_get_text = p_decorate(get_text)

print my_get_text("John")

# <p>Outputs lorem ipsum, John dolor sit amet</p>
```

C'était notre premier décorateur. Une fonction qui prend une autre fonction en argument, génère une nouvelle fonction, augmente le travail de la fonction d'origine et retourne la fonction générée pour pouvoir l'utiliser n'importe où. Pour que get_text soit lui-même décoré par p_decorate, il suffit d'affecter `get_text` au résultat de `p_decorate`.

```python
get_text = p_decorate(get_text)

print get_text("John")

# Outputs lorem ipsum, John dolor sit amet
```

Une autre chose à noter est que notre fonction décorée prend un argument de nom. Tout ce que nous avons à faire dans le décorateur est de laisser le wrapper de get_text passer cet argument.

### Syntaxe du décorateur de Python

Python rend la création et l'utilisation de décorateurs un peu plus propres et plus agréables pour le programmeur grâce à du sucre syntaxique. Pour décorer get_text, nous ne sommes pas obligés. `get_text = p_decorator(get_text)` Il y a un raccourci pour mentionner la fonction de décoration avant la fonction. décoré. Le nom du décorateur doit être accompagné d'un symbole `@`.

```python
def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

@p_decorate
def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)

print get_text("John")

# Outputs <p>lorem ipsum, John dolor sit amet</p>
```

Considérons maintenant que nous voulions décorer notre fonction `get_text` par 2 autres fonctions pour envelopper une balise `div` et `strong` autour de la sortie de la chaîne.

```python
def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

def strong_decorate(func):
    def func_wrapper(name):
        return "<strong>{0}</strong>".format(func(name))
    return func_wrapper

def div_decorate(func):
    def func_wrapper(name):
        return "<div>{0}</div>".format(func(name))
    return func_wrapper
```

Avec l’approche de base, décorer get_text serait dans le sens de

```python
get_text = div_decorate(p_decorate(strong_decorate(get_text)))
```

Avec la syntaxe de décorateur de Python, la même chose peut être obtenue avec beaucoup plus de puissance expressive.

```python
@div_decorate
@p_decorate
@strong_decorate
def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)

print get_text("John")

# Outputs <div><p><strong>lorem ipsum, John dolor sit amet</strong></p></div>
```

Une chose importante à noter ici est que l'ordre de réglage de nos décorateurs est important. Si l'ordre était différent dans l'exemple ci-dessus, la sortie aurait été différente.

### Méthodes de décoration

En Python, les méthodes sont des fonctions qui attendent que leur premier paramètre soit une référence à l'objet en cours. Nous pouvons construire des décorateurs pour les méthodes de la même manière, tout en prenant auto en considération dans la fonction enveloppe.

```python
def p_decorate(func):
   def func_wrapper(*args, **kwargs):
       return "<p>{0}</p>".format(func(*args, **kwargs))
   return func_wrapper

class Person(object):
    def __init__(self):
        self.name = "John"
        self.family = "Doe"

    @p_decorate
    def get_fullname(self):
        return self.name+" "+self.family

my_person = Person()

print my_person.get_fullname()
```

### Transmettre des arguments aux décorateurs

En regardant l'exemple ci-dessus, vous remarquerez à quel point les décorateurs de cet exemple sont redondants. 3 décorateurs (`div_decorate`, `p_decorate`, `strong_decorate`) ayant chacun la même fonctionnalité mais enveloppant la chaîne avec des balises différentes. Nous pouvons certainement faire beaucoup mieux que cela. Pourquoi ne pas avoir une implémentation plus générale pour celui qui prend l'étiquette avec une chaîne? Oui s'il vous plaît!

```python
def tags(tag_name):
    def tags_decorator(func):
        def func_wrapper(name):
            return "<{0}>{1}</{0}>".format(tag_name, func(name))
        return func_wrapper
    return tags_decorator

@tags("p")
def get_text(name):
    return "Hello "+name

print get_text("John")

# Outputs <p>Hello John</p>
```

## Property

Supposons que vous décidiez de créer une classe capable de stocker la température en degrés Celsius. Il implémenterait également une méthode pour convertir la température en degrés Fahrenheit. Une façon de procéder est la suivante.

```python
class Celsius:
    def __init__(self, temperature = 0):
        self.temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32
```

Nous pourrions créer des objets en dehors de cette classe et manipuler l'attribut `temperature` comme nous le souhaitions. Essayez-les sur le shell Python.

```python
>>> # create new object
>>> man = Celsius()

>>> # set temperature
>>> man.temperature = 37

>>> # get temperature
>>> man.temperature
37

>>> # get degrees Fahrenheit
>>> man.to_fahrenheit()
98.60000000000001
```

Maintenant, supposons que notre classe soit devenue populaire parmi les clients et qu'ils ont commencé à l'utiliser dans leurs programmes. Ils ont fait toutes sortes d'affectations à l'objet.

Un jour fatidique, un client de confiance est venu à nous et a suggéré que les températures ne peuvent pas descendre en dessous de -273 degrés Celsius (les étudiants en thermodynamique pourraient prétendre que c'est en fait -273,15), également appelé le zéro absolu. Il nous a également demandé de mettre en œuvre cette contrainte de valeur. En tant que société qui s'efforce de satisfaire les clients, nous avons tenu compte de cette suggestion et publié la version 1.01 (une mise à niveau de notre classe existante).

### Utilisation de Getters et Setters

Une solution évidente à la contrainte ci-dessus sera de cacher l'attribut temperature(le rendre privé) et de définir de nouvelles interfaces getter et setter pour le manipuler. Cela peut être fait comme suit.

```python
class Celsius:
    def __init__(self, temperature = 0):
        self.set_temperature(temperature)

    def to_fahrenheit(self):
        return (self.get_temperature() * 1.8) + 32

    # new update
    def get_temperature(self):
        return self._temperature

    def set_temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        self._temperature = value
```

On peut voir ci - dessus que de nouvelles méthodes `get_temperature()` et `set_temperature()` ont été définis et , en outre, `temperature` a été remplacé par `_temperature`. Un trait de soulignement (_) au début est utilisé pour indiquer les variables privées dans Python.

```python
>>> c = Celsius(-277)
Traceback (most recent call last):
...
ValueError: Temperature below -273 is not possible

>>> c = Celsius(37)
>>> c.get_temperature()
37
>>> c.set_temperature(10)

>>> c.set_temperature(-300)
Traceback (most recent call last):
...
ValueError: Temperature below -273 is not possible
```
Cette mise à jour a correctement implémenté la nouvelle restriction. Nous ne sommes plus autorisés à régler la température en dessous de -273.

Ce refactoring peut causer des problèmes aux clients avec des centaines de milliers de lignes de codes.

Dans l'ensemble, notre nouvelle mise à jour n'était pas compatible avec les versions antérieures. C'est là que la propriété vient à la rescousse.

### Le pouvoir de @property

La manière pythonique de résoudre le problème ci-dessus consiste à utiliser la propriété. Voici comment nous aurions pu y parvenir.

```python
class Celsius:
    def __init__(self, temperature = 0):
        self.temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    def get_temperature(self):
        print("Getting value")
        return self._temperature

    def set_temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value

    temperature = property(get_temperature,set_temperature)
```

La dernière ligne du code crée un objet propriété temperature. En termes simples, la propriété attache du code ( `get_temperature` et `set_temperature`) aux attributs d'attribut member ( `temperature`).

### Creuser plus profondément dans la propriété

En Python, `property()` est une fonction intégrée qui crée et renvoie un objet de propriété. La signature de cette fonction est

```python
property(fget=None, fset=None, fdel=None, doc=None)
```

`fget` est la fonction pour obtenir la valeur de l'attribut, `fset` est la fonction pour définir la valeur de l'attribut, `fdel` est la fonction pour supprimer l'attribut et `doc` est une chaîne (comme un commentaire). Comme vu de l'implémentation, ces arguments de fonction sont facultatifs. Ainsi, un objet de propriété peut simplement être créé comme suit.

```python
>>> property()
<property object at 0x0000000003239B38>
```

Un objet de propriété a trois méthodes, `getter()` et `setter()`, et `deleter()` à spécifier fget, fsetet fdelà un stade ultérieur. Cela signifie que la ligne

```python
temperature = property(get_temperature,set_temperature)

# make empty property
temperature = property()
# assign fget
temperature = temperature.getter(get_temperature)
# assign fset
temperature = temperature.setter(set_temperature)
```

Nous pouvons aller plus loin et ne pas définir les noms `get_temperature` et `set_temperature` comme ils sont inutiles et polluer l'espace de noms de classe. Pour cela, nous réutilisons le nom temperaturetout en définissant nos fonctions getter et setter. Voilà comment cela peut être fait.

```python
class Celsius:
    def __init__(self, temperature = 0):
        self._temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value
```


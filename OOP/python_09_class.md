# Python Objets et Classe

Python est un langage de programmation orienté objet. Contrairement à la programmation orientée procédure, où l'accent est mis sur les fonctions, la contrainte de programmation orientée objet sur les objets.

Object est simplement une collection de données (variables) et de méthodes (fonctions) qui agissent sur ces données. Et, la classe est un modèle pour l'objet.

On peut penser à la classe comme à un croquis (prototype) d'une maison. Il contient tous les détails sur les sols, les portes, les fenêtres, etc. Sur la base de ces descriptions, nous construisons la maison. House est l'objet.

Comme de nombreuses maisons peuvent être créées à partir d'une description, nous pouvons créer de nombreux objets à partir d'une classe. Un objet est aussi appelé instance d'une classe et le processus de création de cet objet s'appelle instanciation .

## Définir une classe en Python

Comme les définitions de fonction commencent par le mot-clé `def` , en Python, nous définissons une classe en utilisant le mot-clé `class` .

La première chaîne est appelée `docstring` et contient une brève description de la classe. Bien que non obligatoire, ceci est recommandé.

Voici une définition de classe simple.

```python
class MyNewClass:
    '''This is a docstring. I have created a new class'''
    pass
```

Une classe crée un nouvel espace de noms local où tous ses attributs sont définis. Les attributs peuvent être des données ou des fonctions.

Il y a aussi des attributs spéciaux qui commencent par des soulignés doubles (`__`). Par exemple, `__doc__` nous donne la documentation de cette classe.

Dès que nous définissons une classe, un nouvel objet de classe est créé avec le même nom. Cet objet de classe nous permet d'accéder aux différents attributs ainsi que d'instancier de nouveaux objets de cette classe.

```python
class MyClass:
    "This is my second class"
    a = 10
    def func(self):
        print('Hello')

# Output: 10
print(MyClass.a)

# Output: <function MyClass.func at 0x0000000003079BF8>
print(MyClass.func)

# Output: 'This is my second class'
print(MyClass.__doc__)
```

Lorsque vous exécutez le programme, la sortie sera la suivante:

```
10
<function 0x7feaa932eae8="" at="" myclass.func="">
This is my second class
```

## Créer un objet en Python

Nous avons vu que l'objet de classe pouvait être utilisé pour accéder à différents attributs.

Il peut également être utilisé pour créer de nouvelles instances d'objet (instanciation) de cette classe. La procédure pour créer un objet est similaire à un appel de fonction .

```python
ob = MyClass()
```

Cela créera un nouvel objet d'instance nommé `ob` . Nous pouvons accéder aux attributs des objets en utilisant le préfixe de nom d'objet.

Les attributs peuvent être des données ou une méthode. La méthode d'un objet est une fonction correspondante de cette classe. Tout objet fonction qui est un attribut de classe définit une méthode pour les objets de cette classe.

Ce moyen de dire, puisque `MyClass.fun` cest un objet fonction (attribut de classe), `ob`.funcsera un objet méthode.

```python
class MyClass:
    "This is my second class"
    a = 10
    def func(self):
        print('Hello')

# create a new MyClass
ob = MyClass()

# Output: <function MyClass.func at 0x000000000335B0D0>
print(MyClass.func)

# Output: <bound method MyClass.func of <__main__.MyClass object at 0x000000000332DEF0>>
print(ob.func)

# Calling function func()
# Output: Hello
ob.func()
```

Vous avez peut-être remarqué le paramètre self dans la définition de la fonction à l'intérieur de la classe, mais nous avons appelé la méthode simplement `ob.func()` sans aucun argument . Ça marchait encore.

En effet, chaque fois qu'un objet appelle sa méthode, l'objet lui-même est transmis en tant que premier argument. Donc, `ob.func()` traduit en `MyClass.func(ob)`.

En général, l'appel d'une méthode avec une liste de n arguments revient à appeler la fonction correspondante avec une liste d'arguments créée en insérant l'objet de la méthode avant le premier argument.

Pour ces raisons, le premier argument de la fonction en classe doit être l'objet lui-même. Ceci est classiquement appelé `self` . Il peut être nommé autrement, mais nous recommandons fortement de suivre la convention.

Maintenant, vous devez vous familiariser avec l'objet de classe, l'objet d'instance, l'objet de fonction, l'objet de méthode et leurs différences.

## Constructeurs en Python

Les fonctions de classe qui commencent par un double trait de soulignement `(__)` sont appelées fonctions spéciales car elles ont une signification particulière.

La fonction `__init__()` est particulièrement intéressante . Cette fonction spéciale est appelée chaque fois qu'un nouvel objet de cette classe est instancié.

Ce type de fonction est également appelé constructeurs en programmation orientée objet (OOP). Nous l'utilisons normalement pour initialiser toutes les variables.

```python
class ComplexNumber:
    def __init__(self,r = 0,i = 0):
        self.real = r
        self.imag = i

    def getData(self):
        print("{0}+{1}j".format(self.real,self.imag))

# Create a new ComplexNumber object
c1 = ComplexNumber(2,3)

# Call getData() function
# Output: 2+3j
c1.getData()

# Create another ComplexNumber object
# and create a new attribute 'attr'
c2 = ComplexNumber(5)
c2.attr = 10

# Output: (5, 0, 10)
print((c2.real, c2.imag, c2.attr))

# but c1 object doesn't have attribute 'attr'
# AttributeError: 'ComplexNumber' object has no attribute 'attr'
c1.attr
```

Dans l'exemple ci-dessus, nous définissons une nouvelle classe pour représenter des nombres complexes. Il a deux fonctions `__init__()` pour initialiser les variables (zéro par défaut) et `getData()` pour afficher le numéro correctement.

Une chose intéressante à noter dans l'étape ci-dessus est que les attributs d'un objet peuvent être créés à la volée. Nous avons créé un nouvel attribut attr pour l'objet `c2` et nous le lisons également. Mais cela n'a pas créé cet attribut pour l'objet `c1` .

## Suppression d'attributs et d'objets

Tout attribut d'un objet peut être supprimé à tout moment, à l'aide de l'instruction del. Essayez ce qui suit sur le shell Python pour voir le résultat.

```python
c1 = ComplexNumber(2,3)
del c1.imag
c1.getData()
Traceback (most recent call last):
...
AttributeError: 'ComplexNumber' object has no attribute 'imag'

del ComplexNumber.getData
c1.getData()
Traceback (most recent call last):
...
AttributeError: 'ComplexNumber' object has no attribute 'getData'
```

Nous pouvons même supprimer l'objet lui-même, en utilisant l'instruction del.

```python
c1 = ComplexNumber(1,3)
del c1
c1
Traceback (most recent call last):
...
NameError: name 'c1' is not defined
```

En fait, c'est plus compliqué que ça. Dans ce cas `c1 = ComplexNumber(1,3)`, un nouvel objet d’instance est créé en mémoire et le nom `c1` s’y lie.

Sur la commande del `c1`, cette liaison est supprimée et le nom `c1` est supprimé de l'espace de noms correspondant. L'objet continue cependant d'exister en mémoire et si aucun autre nom n'y est lié, il est automatiquement détruit ultérieurement.

Cette destruction automatique des objets non référencés dans Python est également appelée récupération de mémoire.

## Attribut et méthodes privés

Dans le contexte de la classe, private signifie que les attributs ne sont disponibles que pour les membres de la classe et non pour l'extérieur de la classe.

Supposons que nous ayons la classe suivante qui a des attributs privés  `__alias` :

```python
class Person:
   def __init__(self, name, alias):
      self.name = name       # public
      self.__alias = alias   # private

   def who(self):
      print('name  : ', self.name)
      print('alias : ', self.__alias)
```

Nous créons une instance de classe `Person`, puis essayons d'accéder à ses attributs, que ce soit public ou privé:

```Python
>>> x = Person(name='Alex', alias='rockstar')
>>> x.name
'Alex'
>>> x.alias
Traceback (most recent call last):
  File "", line 1, in
AttributeError: Person instance has no attribute 'alias'
```

Pour l'attribut public `name`, nous pouvons accéder via une variable d'instance, mais pas pour l'attribut privé `alias`. Même nous essayons ceci:

```Python
>>> x.__alias
Traceback (most recent call last):
  File "", line 1, in
AttributeError: P instance has no attribute '__alias'
```

nous n'avons toujours pas le droit d'y accéder.

Mais voici un besoin de magie. Un trait de soulignement ('_') avec le nom de la classe fera de la magie:

```Python
>>> x._Person__alias
'rockstar'
```

L'appel suivant fonctionne également comme prévu:

```python
>>> x.who()
('name  : ', 'Alex')
('alias : ', 'rockstar')
```

Nous allons utiliser le même code que dans la section précédente mais nous allons ajouter deux méthodes: les méthodes foo () et __foo ():

```python
class Person:
   def __init__(self, name, alias):
      self.name = name       # public
      self.__alias = alias   # private

   def who(self):
      print('name  : ', self.name)
      print('alias : ', self.__alias)

   def __foo(self):          # private method
      print('This is private method')

   def foo(self):            # public method
      print('This is public method')
      self.__foo()
```

Comment pouvons-nous utiliser la méthode privée? Si nous essayons:

```python
>>> x = Person('Alex', 'rockstar')
>>> x.__foo()
Traceback (most recent call last):
  File "", line 1, in 
AttributeError: P instance has no attribute '__foo'
```

La bonne façon d'accéder à la méthode privée est la suivante:

```python
>>> x._Person__foo()
This is private method
```

Bien sûr, appeler la méthode privée via le public fonctionnera comme prévu:

```python
>>> x.foo()
This is public emthod
This is private method
```

## Monkey Patching

"Monkey Patching" signifie l'ajout d'une nouvelle variable ou méthode à une classe après sa définition. Par exemple, disons que nous avons défini la classe A comme

```python
class A(object):
    def __init__(self, num):
        self.num = num
    def __add__(self, other):
        return A(self.num + other.num)
```

Mais maintenant, nous voulons ajouter une autre fonction plus tard dans le code. Supposons que cette fonction est la suivante.

```python
def get_num(self):
    return self.num
```

Mais comment ajouter cela comme méthode dans A? C'est simple, nous plaçons essentiellement cette fonction dans A avec une déclaration d'affectation.

```python
A.get_num = get_num
```

Pourquoi ça marche? Parce que les fonctions sont des objets comme tout autre objet, et les méthodes sont des fonctions qui appartiennent à la classe.
La fonction get_num doit être disponible pour tous les existants (déjà créés) ainsi que pour les nouvelles instances de A
Ces ajouts sont disponibles automatiquement sur toutes les instances de cette classe (ou de ses sous-classes). Par exemple

```python
foo = A(42)
A.get_num = get_num
bar = A(6);
foo.get_num() # 42
bar.get_num() # 6
```


# Python Introspection

En programmation informatique, l'introspection se réfère à la capacité d'examiner quelque chose pour déterminer ce que c'est, ce qu'il sait et ce qu'il est capable de faire. L'introspection donne aux programmeurs beaucoup de flexibilité et de contrôle. Une fois que vous avez travaillé avec un langage de programmation prenant en charge l'introspection, vous pouvez également penser que "l'objet non examiné ne vaut pas l'instanciation".

Commençons donc notre enquête, en utilisant Python interactivement. Lorsque nous démarrons Python à partir de la ligne de commande, nous entrons dans le shell Python, où nous pouvons entrer du code Python et obtenir une réponse immédiate de l'interpréteur Python. (Les commandes répertoriées dans cet article s'exécuteront correctement avec Python 2.2.2. Vous pouvez obtenir des résultats ou des erreurs différents si vous utilisez une version antérieure. Vous pouvez télécharger la dernière version à partir du site Web Python [voir Rubriques connexes ].)

```Shell
$ python
Python 2.2.2 (#1, Oct 28 2002, 17:22:19)
[GCC 3.2 (Mandrake Linux 9.0 3.2-1mdk)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Une fois que Python est en cours d'exécution et que vous consultez l'invite Python `>>>`, vous vous demandez peut-être quels mots seront reconnus par Python. La plupart des langages de programmation ont des mots réservés, ou des mots-clés, qui ont une signification particulière dans cette langue, et Python ne fait pas exception. Vous avez peut-être également remarqué que Python nous a suggéré de taper `help` pour obtenir plus d'informations. Peut-être pouvons-nous demander de l'aide à Python sur les mots-clés.

## Utilitaire d'aide en ligne de Python

Commençons par taper `help`, comme suggéré, et voir si cela nous donne des indices sur les mots-clés:

```Python
>>> help
Type help() for interactive help, or help(object) for help about object.
```

Puisque nous ne savons pas quel objet peut contenir des mots-clés, essayons help()sans spécifier aucun objet particulier:

```Python
>>> help()
 
Welcome to Python 2.2!  This is the online help utility.
 
If this is your first time using Python, you should definitely check out
the tutorial on the Internet at http://www.python.org/doc/tut/.
 
Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules.  To quit this help utility and
return to the interpreter, just type "quit".
 
To get a list of available modules, keywords, or topics, type "modules",
"keywords", or "topics".  Each module also comes with a one-line summary
of what it does; to list the modules whose summaries contain a given word
such as "spam", type "modules spam".
 
help>
```

Nous semblons nous rapprocher, maintenant. Entrons `keywords` à l'invite d'aide:

```Python
help> keywords
 
Here is a list of the Python keywords.  Enter any keyword to get more help.
 
and                 elif                global              or
assert              else                if                  pass
break               except              import              print
class               exec                in                  raise
continue            finally             is                  return
def                 for                 lambda              try
del                 from                not                 while
 
help> quit
 
You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)".  Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.
>>>
```

Liste partielle des modules disponibles

```Python
>>> help('modules')
 
Please wait a moment while I gather a list of all available modules...
 
BaseHTTPServer      cgitb               marshal             sndhdr
Bastion             chunk               math                socket
CDROM               cmath               md5                 sre
CGIHTTPServer       cmd                 mhlib               sre_compile
Canvas              code                mimetools           sre_constants
    <...>
bisect              macpath             signal              xreadlines
cPickle             macurl2path         site                xxsubtype
cStringIO           mailbox             slgc (package)      zipfile
calendar            mailcap             smtpd
cgi                 markupbase          smtplib
 
Enter any module name to get more help.  Or, type "modules spam" to search
for modules whose descriptions contain the word "spam".
 
>>>
```

## Le module sys

Le module `sys` est un module qui fournit des informations pertinentes sur Python. Le module `sys` contient une variété de variables et de fonctions qui révèlent des détails intéressants sur l'interpréteur Python actuel. Jetons un coup d'oeil à certains d'entre eux. Encore une fois, nous allons exécuter Python de manière interactive et entrer des commandes à l'invite de commandes Python. La première chose à faire est d'importer le module `sys`. Ensuite, nous entrerons la sys.executable qui contient le chemin de l'interpréteur Python:

```Shell
$ python
Python 2.2.2 (#1, Oct 28 2002, 17:22:19)
[GCC 3.2 (Mandrake Linux 9.0 3.2-1mdk)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import sys
>>> sys.executable
'/usr/local/bin/python'
```

Regardons quelques autres attributs utiles du module `sys`.

La variable `platform` nous indique sur quel système d'exploitation nous sommes:

L'attribut `sys.platform`

```Python
>>> sys.platform
'linux2'
```

La version actuelle de Python est disponible sous forme de chaîne et de tuple (un tuple contient une séquence d'objets):

```Python
>>> sys.version
'2.2.2 (#1, Oct 28 2002, 17:22:19) \n[GCC 3.2 (Mandrake Linux 9.0 3.2-1mdk)]'
>>> sys.version_info
(2, 2, 2, 'final', 0)
```

La variable `maxint` reflète la valeur entière la plus élevée disponible:

```python
>>> sys.maxint
2147483647
```

La variable `argv` est une liste contenant des arguments de ligne de commande, le cas échéant. Le premier élément, `argv[0]`, est le chemin du script exécuté. Lorsque nous exécutons Python de manière interactive, cette valeur est une chaîne vide:

```python
>>> sys.argv
['']
```

La variable `path` est le chemin de recherche du module, la liste des répertoires dans lesquels Python recherchera des modules lors des importations. La chaîne vide '', dans la première position, fait référence au répertoire en cours:

```Python
>>> sys.path
['', '/usr/lib/python2.7', '/usr/lib/python2.7/plat-x86_64-linux-gnu', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/home/amastour/.local/lib/python2.7/site-packages', '/usr/local/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/gtk-2.0']
```

La variable `modules` est un dictionnaire qui mappe les noms de module aux objets de module pour tous les modules actuellement chargés. Comme vous pouvez le voir, Python charge certains modules par défaut:

```Python
{'stat': <module 'stat' from '/usr/local/lib/python2.2/stat.pyc'>,
'__future__': <module '__future__' from '/usr/local/lib/python2.2/__future__.pyc'>,
'copy_reg': <module 'copy_reg' from '/usr/local/lib/python2.2/copy_reg.pyc'>,
'posixpath': <module 'posixpath' from '/usr/local/lib/python2.2/posixpath.pyc'>,
'UserDict': <module 'UserDict' from '/usr/local/lib/python2.2/UserDict.pyc'>,
'signal': <module 'signal' (built-in)>,
'site': <module 'site' from '/usr/local/lib/python2.2/site.pyc'>,
'__builtin__': <module '__builtin__' (built-in)>,
'sys': <module 'sys' (built-in)>,
'posix': <module 'posix' (built-in)>,
'types': <module 'types' from '/usr/local/lib/python2.2/types.pyc'>,
'__main__': <module '__main__' (built-in)>,
'exceptions': <module 'exceptions' (built-in)>,
'os': <module 'os' from '/usr/local/lib/python2.2/os.pyc'>,
'os.path': <module 'posixpath' from '/usr/local/lib/python2.2/posixpath.pyc'>}
```

## La fonction  dir()

Bien qu'il soit relativement facile de trouver et d'importer un module, il n'est pas facile de se souvenir de ce que contient chaque module. Et vous ne voulez pas toujours avoir à regarder le code source pour le savoir. Heureusement, Python permet d'examiner le contenu des modules (et d'autres objets) à l'aide de la fonction intégrée `dir()`.

La fonction `dir()` est probablement le plus connu des mécanismes d'introspection de Python. Il renvoie une liste triée de noms d'attribut pour tout objet qui lui est transmis. Si aucun objet n'est spécifié, `dir()` renvoie les noms dans la portée actuelle. Appliquons d`ir()` à notre module `keyword` et voyons ce qu'il révèle:

```Python
>>> dir(keyword)
['__all__', '__builtins__', '__doc__', '__file__', '__name__',
'iskeyword', 'keyword', 'kwdict', 'kwlist', 'main']
```

```python
>>> dir()
['__builtins__', '__doc__', '__name__', 'keyword', 'sys']
```

Nous avons mentionné que la dir()fonction était une fonction intégrée, ce qui signifie que nous n'avons pas besoin d'importer un module pour utiliser la fonction. Python reconnaît les fonctions intégrées sans avoir à faire quoi que ce soit. Et maintenant, nous voyons ce nom `__builtins__`, renvoyé par un appel à `dir().` Peut-être y a-t-il un lien ici. Entrons le nom `__builtins__` à l'invite Python et voyons si Python nous dit quelque chose d'intéressant à ce sujet:

```python
>>> __builtins__
<module '__builtin__' (built-in)>
```

`__builtins__` semble donc y avoir un nom dans l'étendue actuelle qui est lié à l'objet de module nommé `__builtin_`_. Notez que si vous recherchez un `__builtin__.py` fichier sur le disque, vous vous retrouverez les mains vides. Cet objet de module particulier est créé par l'interpréteur Python, car il contient des éléments toujours disponibles pour l'interpréteur. Et bien qu'il n'y ait pas de fichier physique à examiner, nous pouvons toujours appliquer notre fonction `dir()` à cet objet pour voir toutes les fonctions intégrées, les objets d'erreur et quelques attributs divers qu'il contient:

```python
>>> dir(__builtins__)
['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BufferError', 'BytesWarning', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FloatingPointError', 'FutureWarning', 'GeneratorExit','IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'NameError', 'None', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'ReferenceError', 'RuntimeError', 'RuntimeWarning', 'StandardError', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'ZeroDivisionError', '_', '__debug__', '__doc__', '__import__', '__name__', '__package__', 'abs', 'all', 'any', 'apply', 'basestring', 'bin', 'bool', 'buffer', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'cmp', 'coerce', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod', 'enumerate', 'eval', 'execfile', 'exit', 'file', 'filter', 'float', 'format', 'frozenset', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'intern', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'long', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'quit', 'range', 'raw_input', 'reduce', 'reload', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum','super', 'tuple', 'type', 'unichr', 'unicode', 'vars', 'xrange', 'zip']
```

La fonction `dir()` fonctionne sur tous les types d'objet, y compris les chaînes, les entiers, les listes, les tuples, les dictionnaires, les fonctions, les classes personnalisées, les instances de classe et les méthodes de classe. Appliquons `dir()` à un objet chaîne et voyons ce que Python renvoie. Comme vous pouvez le voir, même une simple chaîne Python possède un certain nombre d'attributs:

```python
>>> dir('this is a string')
['__add__', '__class__', '__contains__', '__delattr__', '__doc__', '__eq__',
'__ge__', '__getattribute__', '__getitem__', '__getslice__', '__gt__',
'__hash__', '__init__', '__le__', '__len__', '__lt__', '__mul__', '__ne__',
'__new__', '__reduce__', '__repr__', '__rmul__', '__setattr__', '__str__',
'capitalize', 'center', 'count', 'decode', 'encode', 'endswith', 'expandtabs',
'find', 'index', 'isalnum', 'isalpha', 'isdigit', 'islower', 'isspace',
'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'replace', 'rfind',
'rindex', 'rjust', 'rstrip', 'split', 'splitlines', 'startswith', 'strip',
'swapcase', 'title', 'translate', 'upper', 'zfill']
```

Essayez vous-même les exemples suivants pour voir ce qu'ils reviennent.

```python

dir(42)   # Integer (and the meaning of life)
dir([])   # List (an empty list, actually)
dir(())   # Tuple (also empty)
dir({})   # Dictionary (ditto)
dir(dir)  # Function (functions are also objects)
```

## Chaînes de documentation

Un attribut que vous avez peut-être remarqué dans beaucoup de nos exemples `dir()` est l'attribut `__doc__` . Cet attribut est une chaîne contenant les commentaires décrivant un objet. Python appelle cela une chaîne de documentation, ou docstring, et voici comment cela fonctionne. Si la première instruction d'une définition de module, de classe, de méthode ou de fonction est une chaîne, cette chaîne est associée à l'objet en tant qu'attribut  `__doc__` . Par exemple, examinez le docstring de l'objet `__builtins__`. Nous utiliserons la printdéclaration de Python pour rendre la sortie plus facile à lire, puisque docstrings contient souvent des nouvelles lignes intégrées ( \n):

```python
>>> print __builtins__.__doc__   # Module docstring
Built-in functions, exceptions, and other objects.
 
Noteworthy: None is the `nil' object; Ellipsis represents `...' in slices.
```

Encore une fois, Python conserve même les docstrings sur les classes et les méthodes définies de manière interactive dans le shell Python. Regardons les docstrings pour notre classe Person et sa introméthode:

```python
>>> class Person(object):
...     """Person class."""
...     def __init__(self, name, age):
...         self.name = name
...         self.age = age
...     def intro(self):
...         """Return an introduction."""
...         return "Hello, my name is %s and I'm %s." % (self.name, self.age)
...
>>> Person.__doc__         # Class docstring
'Person class.'
>>> Person.intro.__doc__   # Class method docstring
'Return an introduction.'
```

## Interroger des objets Python

Comme les objets du monde réel, plusieurs objets informatiques peuvent partager des caractéristiques communes tout en conservant leurs propres variations mineures. Pensez aux livres que vous voyez dans une librairie. Chaque copie physique d'un livre peut comporter une bavure, quelques pages déchirées ou un numéro d'identification unique. Et bien que chaque livre soit un objet unique, chaque livre ayant le même titre est simplement une instance d'un modèle original et conserve la plupart des caractéristiques de l'original.

Il en va de même pour les classes orientées objet et les instances de classe. Par exemple, chaque chaîne Python est dotée des attributs que nous avons vus révélés par la dir()fonction. Et dans un exemple précédent, nous avons défini notre propre Personclasse, qui servait de modèle pour créer des instances individuelles de personne, chacune ayant son propre nom et ses propres valeurs d'âge, tout en partageant la possibilité de se présenter. C'est une orientation objet.


En termes informatiques, les objets sont donc des choses qui ont une identité et une valeur, qui sont d'un certain type, possèdent certaines caractéristiques et se comportent d'une certaine manière. Et les objets héritent d'un grand nombre de leurs attributs d'une ou plusieurs classes parentes. Autre que des mots clés et des symboles spéciaux (comme les opérateurs, tels que +, -, *, **, /, %, <, >, etc.) tout en Python est un objet. Et Python est livré avec un ensemble riche de types d’objets: chaînes de caractères, entiers, flottants, listes, tuples, dictionnaires, fonctions, classes, instances de classes, modules, fichiers, etc.

### Nom

Tous les objets n'ont pas de nom, mais pour ceux qui le font, le nom est stocké dans leur attribut `__name__`. Notez que le nom est dérivé de l'objet, pas la variable qui référence l'objet. L'exemple suivant met en évidence cette distinction:

```python
, "credits" or "license" for more information.
>>> dir()                # The dir() function
['__builtins__', '__doc__', '__name__']
>>> directory = dir      # Create a new variable
>>> directory()          # Works just like the original object
['__builtins__', '__doc__', '__name__', 'directory']
>>> dir.__name__         # What's your name?
'dir'
>>> directory.__name__   # My name is the same
'dir'
>>> __name__             # And now for something completely different
'__main__'
```

## Type

La type()fonction nous aide à déterminer si un objet est une chaîne, un entier ou un autre type d'objet. Cela se fait en renvoyant un objet de type, qui peut être comparé aux types définis dans le module `types`:

```python
>>> import types
>>> print types.__doc__
Define names for all type symbols known in the standard interpreter.
 
Types that are part of optional modules (e.g. array) are not listed.
 
>>> dir(types)
['BufferType', 'BuiltinFunctionType', 'BuiltinMethodType', 'ClassType',
'CodeType', 'ComplexType', 'DictProxyType', 'DictType', 'DictionaryType',
'EllipsisType', 'FileType', 'FloatType', 'FrameType', 'FunctionType',
'GeneratorType', 'InstanceType', 'IntType', 'LambdaType', 'ListType',
'LongType', 'MethodType', 'ModuleType', 'NoneType', 'ObjectType', 'SliceType',
'StringType', 'StringTypes', 'TracebackType', 'TupleType', 'TypeType',
'UnboundMethodType', 'UnicodeType', 'XRangeType', '__builtins__', '__doc__',
'__file__', '__name__']
>>> s = 'a sample string'
>>> type(s)
<type 'str'>
>>> if type(s) is types.StringType: print "s is a string"
...
s is a string
>>> type(42)
<type 'int'>
>>> type([])
<type 'list'>
>>> type({})
<type 'dict'>
>>> type(dir)
<type 'builtin_function_or_method'>
```

### Les attributs

Nous avons vu que les objets ont des attributs et que la fonction `dir()` renvoie une liste de ces attributs. Parfois, cependant, nous voulons simplement tester l'existence d'un ou plusieurs attributs. Et si un objet a l'attribut en question, nous voulons souvent récupérer cet attribut. Ces tâches sont gérées par les fonctions `hasattr()` et `getattr()`, comme illustré dans cet exemple:

```python
>> print hasattr.__doc__
hasattr(object, name) -> Boolean
 
Return whether the object has an attribute with the given name.
(This is done by calling getattr(object, name) and catching exceptions.)
>>> print getattr.__doc__
getattr(object, name[, default]) -> value
 
Get a named attribute from an object; getattr(x, 'y') is equivalent to x.y.
When a default argument is given, it is returned when the attribute doesn't
exist; without it, an exception is raised in that case.
>>> hasattr(id, '__doc__')
1
>>> print getattr(id, '__doc__')
id(object) -> integer
 
Return the identity of an object.  This is guaranteed to be unique among
simultaneously existing objects.  (Hint: it's the object's memory address.)
```

### Les callables

Les objets qui représentent un comportement potentiel (fonctions et méthodes) peuvent être appelés ou appelés. Nous pouvons tester la callabilité d'un objet avec la fonction `callable()`:

```python
>>> print callable.__doc__
callable(object) -> Boolean
 
Return whether the object is callable (i.e., some kind of function).
Note that classes are callable, as are instances with a __call__() method.
>>> callable('a string')
0
>>> callable(dir)
1
```

### Instances

Alors que la fonction `type()` nous a donné le type d'un objet, nous pouvons également tester un objet pour déterminer s'il s'agit d'une instance d'un type particulier ou d'une classe personnalisée utilisant la fonction `isinstance()`:

```python
>>> print isinstance.__doc__
isinstance(object, class-or-type-or-tuple) -> Boolean
 
Return whether an object is an instance of a class or of a subclass thereof.
With a type as second argument, return whether that is the object's type.
The form using a tuple, isinstance(x, (A, B, ...)), is a shortcut for
isinstance(x, A) or isinstance(x, B) or ... (etc.).
>>> isinstance(42, str)
0
>>> isinstance('a string', int)
0
>>> isinstance(42, int)
1
>>> isinstance('a string', str)
1
```

### Des sous-classes

Nous avons mentionné précédemment que les instances d'une classe personnalisée héritent de leurs attributs de la classe. Au niveau de la classe, une classe peut être définie en termes d'une autre classe, et héritera également des attributs de manière hiérarchique. Python prend même en charge l'héritage multiple, ce qui signifie qu'une classe individuelle peut être définie en termes d'héritage et hérite de plusieurs classes parentes. La issubclass()fonction nous permet de savoir si une classe hérite d’une autre:

```python
>>> print issubclass.__doc__
issubclass(C, B) -> Boolean
 
Return whether class C is a subclass (i.e., a derived class) of class B.
>>> class SuperHero(Person):   # SuperHero inherits from Person...
...     def intro(self):       # but with a new SuperHero intro
...         """Return an introduction."""
...         return "Hello, I'm SuperHero %s and I'm %s." % (self.name, self.age)
...
>>> issubclass(SuperHero, Person)
1
>>> issubclass(Person, SuperHero)
0
>>>
```


https://www.ibm.com/developerworks/library/l-pyint/index.html
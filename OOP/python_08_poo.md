# Introduction aux POO en Python

Python est un langage de programmation multi-paradigmes. Ce qui signifie, il supporte une approche de programmation différente.

L'une des approches populaires pour résoudre un problème de programmation consiste à créer des objets. Ceci s'appelle la programmation orientée objet (OOP).

Un objet a deux caractéristiques:

- les attributs
- comportement

Prenons un exemple:

Le perroquet est un objet,

- nom, âge, couleur sont des attributs
- chanter, danser sont des comportements

Le concept de POO dans Python se concentre sur la création de code réutilisable. Ce concept est également connu sous le nom de DRY (Don't Repeat Yourself).

En Python, le concept de POO suit quelques principes de base:
|    | | |
|---------|-----------|--------|
| Héritage |Un processus d'utilisation des détails d'une nouvelle classe sans modifier la classe existante.|
|Encapsulation |Masquer les détails privés d'une classe à partir d'autres objets.|
|Polymorphisme |Un concept d'utilisation de fonctionnement commun de différentes manières pour différentes entrées de données.|

## Classe

Une classe est un modèle pour l'objet.

On peut penser à la classe comme à l’esquisse d’un perroquet avec des étiquettes. Il contient tous les détails sur le nom, les couleurs, la taille, etc. Sur la base de ces descriptions, nous pouvons étudier le perroquet. Ici, le perroquet est un objet.

L'exemple de classe de perroquet peut être:

```python
class Parrot:
    pass
```

Ici, nous utilisons le mots-clés `class` pour définir une classe vide `Parrot` . De classe, nous construisons des instances. Une instance est un objet spécifique créé à partir d'une classe particulière.

## Objet

Un objet (instance) est une instanciation d'une classe. Lorsque class est défini, seule la description de l'objet est définie. Par conséquent, aucune mémoire ou stockage n'est alloué.

L'exemple pour l'objet de la classe de perroquet peut être:

```python
obj = Parrot()
```

Ici, obj est objet de classe Parrot.

Supposons que nous ayons des détails sur le perroquet. Nous allons maintenant montrer comment construire la classe et les objets de perroquet.

Exemple 1: Création d'une classe et d'un objet en Python

```python
class Parrot:

    # class attribute
    species = "bird"

    # instance attribute
    def __init__(self, name, age):
        self.name = name
        self.age = age

# instantiate the Parrot class
blu = Parrot("Blu", 10)
woo = Parrot("Woo", 15)

# access the class attributes
print("Blu is a {}".format(blu.__class__.species))
print("Woo is also a {}".format(woo.__class__.species))

# access the instance attributes
print("{} is {} years old".format( blu.name, blu.age))
print("{} is {} years old".format( woo.name, woo.age))
```

Lorsque nous exécutons le programme, le résultat sera:

```
Blu est un oiseau
Woo est aussi un oiseau
Blu a 10 ans
Woo a 15 ans
```

Dans le programme ci-dessus, nous créons une classe nommée `Parrot` . Ensuite, nous définissons des attributs. Les attributs sont une caractéristique d'un objet.

Ensuite, nous créons des instances de la classe `Parrot` . Ici, `blu` et `woo` sont des références (valeur) à nos nouveaux objets.

Ensuite, nous accédons à l'attribut de classe en utilisant `__class __.species`. Les attributs de classe sont identiques pour toutes les instances d'une classe. De même, nous accédons aux attributs d'instance à l'aide de `blu.nameet` `blu.age`. Cependant, les attributs d'instance sont différents pour chaque instance d'une classe.

## Les méthodes
Les méthodes sont des fonctions définies dans le corps d'une classe. Ils sont utilisés pour définir les comportements d'un objet.

Exemple 2: Création de méthodes en Python

```python
class Parrot:
    
    # instance attributes
    def __init__(self, name, age):
        self.name = name
        self.age = age
    
    # instance method
    def sing(self, song):
        return "{} sings {}".format(self.name, song)

    def dance(self):
        return "{} is now dancing".format(self.name)

# instantiate the object
blu = Parrot("Blu", 10)

# call our instance methods
print(blu.sing("'Happy'"))
print(blu.dance())
```

Lorsque nous exécutons le programme, le résultat sera:

```
Blu sings 'Happy'
Blu is now dancing

```

Dans le programme ci-dessus, nous définissons deux méthodes, à savoir `sing()` et `dance()`. Celles-ci sont appelées méthode d'instance car elles sont appelées sur un objet instance, c.-à-d `blu`.

## Héritage

L'héritage est un moyen de créer une nouvelle classe pour utiliser les détails d'une classe existante sans la modifier. La classe nouvellement formée est une classe dérivée (ou classe enfant). De même, la classe existante est une classe de base (ou une classe parente).

Exemple 3: Utilisation de l'héritage dans Python

```python
# parent class
class Bird:
    
    def __init__(self):
        print("Bird is ready")

    def whoisThis(self):
        print("Bird")

    def swim(self):
        print("Swim faster")

# child class
class Penguin(Bird):

    def __init__(self):
        # call super() function
        super().__init__()
        print("Penguin is ready")

    def whoisThis(self):
        print("Penguin")

    def run(self):
        print("Run faster")

peggy = Penguin()
peggy.whoisThis()
peggy.swim()
peggy.run()
```

Lorsque nous exécutons le programme, le résultat sera:

```
Bird is ready
Penguin is ready
Penguin
Swim faster
Run faster
```

Dans le programme ci-dessus, nous avons créé deux classes, à savoir `Bird` (classe parent) et `Penguin` (classe enfant). La classe enfant hérite des fonctions de la classe parente. Nous pouvons voir cela de méthode `swim()`. Encore une fois, la classe enfant a modifié le comportement de la classe parente. Nous pouvons voir cela de la méthode `whoisThis()`. De plus, nous étendons les fonctions de la classe parente en créant une nouvelle méthode `run()`.

De plus, nous utilisons la fonction `super()` avant la méthode `__init__()`. C'est parce que nous voulons extraire le contenu de la méthode `__init__()` de la classe parente dans la classe enfant.

## Encapsulation

En utilisant la POO en Python, nous pouvons restreindre l'accès aux méthodes et aux variables. Cela empêche les données de modification directe appelée encapsulation. En Python, nous désignons un attribut privé en utilisant le trait de soulignement comme préfixe, c'est-à-dire un simple «_» ou un double «__».

Exemple 4: Encapsulation de données en Python

```python
class Computer:

    def __init__(self):
        self.__maxprice = 900

    def sell(self):
        print("Selling Price: {}".format(self.__maxprice))

    def setMaxPrice(self, price):
        self.__maxprice = price

c = Computer()
c.sell()

# change the price
c.__maxprice = 1000
c.sell()

# using setter function
c.setMaxPrice(1000)
c.sell()
```

Lorsque nous exécutons ce programme, le résultat sera:

```
Selling Price: 900
Selling Price: 900
Selling Price: 1000
```

Dans le programme ci-dessus, nous avons défini un ordinateur de classe . Nous utilisons la méthode `__init__()` pour stocker le prix de vente maximum de l'ordinateur. Nous avons essayé de modifier le prix. Cependant, nous ne pouvons pas le changer car Python traite le `__maxprice` comme des attributs privés. Pour changer la valeur, nous avons utilisé une fonction setter, c'est-à-dire `setMaxPrice()` qui prend le prix comme paramètre.

## Polymorphisme

Le polymorphisme est une capacité (en POO) d'utiliser une interface commune pour plusieurs formes (types de données).

Supposons que nous ayons besoin de colorer une forme, il existe plusieurs options de forme (rectangle, carré, cercle). Cependant, nous pourrions utiliser la même méthode pour colorer n'importe quelle forme. Ce concept s'appelle le polymorphisme.

Exemple 5: Utilisation du polymorphisme en Python

```python
class Parrot:

    def fly(self):
        print("Parrot can fly")
    
    def swim(self):
        print("Parrot can't swim")

class Penguin:

    def fly(self):
        print("Penguin can't fly")
    
    def swim(self):
        print("Penguin can swim")

# common interface
def flying_test(bird):
    bird.fly()

#instantiate objects
blu = Parrot()
peggy = Penguin()

# passing the object
flying_test(blu)
flying_test(peggy)
```

Lorsque nous exécutons le programme ci-dessus, le résultat sera:

```
Parrot can fly
Penguin can't fly
```

Dans le programme ci-dessus, nous avons défini deux classes `Parrot` et `Penguin` . Chacun d'entre eux ont une méthode commune `fly()`. Cependant, leurs fonctions sont différentes. Pour permettre le polymorphisme, nous avons créé une interface commune, c'est-à-dire une fonction `flying_test()` pouvant prendre n'importe quel objet. Ensuite, nous avons passé les objets `blu` et `peggy` dans la fonction `flying_test()`, cela a fonctionné efficacement
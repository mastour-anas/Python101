# Python exception

Python a de nombreuses exceptions intégrées  qui obligent votre programme à générer une erreur lorsque quelque chose ne va pas.

Lorsque ces exceptions se produisent, le processus en cours s'arrête et passe au processus appelant jusqu'à ce qu'il soit traité. Si ce n'est pas géré, notre programme va planter.

Par exemple, si la fonction `A` appelle la fonction `B` qui à son tour appelle la fonction Cet une exception se produit dans la fonction `C`. Si elle n'est pas gérée `C`, l'exception passe à Bet ensuite à `A`.

S'il n'est jamais géré, un message d'erreur est recraché et notre programme s'arrête soudainement et de manière inattendue.

## Attraper des exceptions en Python

En Python, les exceptions peuvent être gérées à l'aide d'une instruction `try`.

Une opération critique pouvant générer une exception est placée dans la clause `try` et le code qui gère l'exception est écrit dans la clause except.

C'est à nous, quelles opérations nous effectuons une fois que nous avons pris l'exception. Voici un exemple simple.

```python
# import module sys to get the type of exception
import sys

randomList = ['a', 0, 2]

for entry in randomList:
    try:
        print("The entry is", entry)
        r = 1/int(entry)
        break
    except:
        print("Oops!",sys.exc_info()[0],"occured.")
        print("Next entry.")
        print()
print("The reciprocal of",entry,"is",r)
```

```
The entry is a
Oops! <class 'ValueError'> occured.
Next entry.

The entry is 0
Oops! <class 'ZeroDivisionError' > occured.
Next entry.

The entry is 2
The reciprocal of 2 is 0.5
```

Dans ce programme, nous bouclons jusqu'à ce que l'utilisateur entre un entier qui a une réciproque valide. La partie qui peut provoquer une exception est placée à l'intérieur du bloc `try`.

Si aucune exception ne se produit, sauf que le bloc est ignoré et que le flux normal continue. Mais si une exception se produit, elle est interceptée par le bloc sauf.

Ici, nous imprimons le nom de l'exception à l'aide de la fonction `ex_info()` dans le module `sys` et demandons à l'utilisateur de réessayer. Nous pouvons voir que les valeurs 'a' et '1.3' provoquent ValueError et que '0' provoque `ZeroDivisionError`.

## Attraper des exceptions spécifiques en Python

Dans la programmation Python, des exceptions sont levées lorsque des erreurs correspondantes se produisent au moment de l'exécution, mais nous pouvons le forcer à utiliser le mot-clé `raise`.

Nous pouvons également éventuellement passer la valeur à l'exception pour clarifier pourquoi cette exception a été soulevée.

```Python
>>> raise KeyboardInterrupt
Traceback (most recent call last):
...
KeyboardInterrupt

>>> raise MemoryError("This is an argument")
Traceback (most recent call last):
...
MemoryError: This is an argument

>>> try:
...     a = int(input("Enter a positive integer: "))
...     if a <= 0:
...         raise ValueError("That is not a positive number!")
... except ValueError as ve:
...     print(ve)
...    
Enter a positive integer: -2
That is not a positive number!
```

## try...finally

L'instruction try en Python peut avoir une clause `finally` facultative . Cette clause est exécutée quelle que soit la nature et est généralement utilisée pour libérer des ressources externes.

Par exemple, nous pouvons être connectés à un centre de données distant via le réseau ou travailler avec un fichier ou travailler avec une interface utilisateur graphique (GUI).

Dans toutes ces circonstances, nous devons nettoyer la ressource une fois qu’elle a été utilisée, qu’elle ait été réussie ou non. Ces actions (fermeture d'un fichier, interface graphique ou déconnexion du réseau) sont effectuées dans la clause `finally` pour garantir l'exécution.

Voici un exemple d' opérations sur les fichiers pour illustrer cela.

```Python
try:
   f = open("test.txt",encoding = 'utf-8')
   # perform file operations
finally:
   f.close()
```

Ce type de construction vérifie que le fichier est fermé même si une exception se produit.

## EXCEPTION DÉFINIE PAR L'UTILISATEUR PYTHON

Python a de nombreuses exceptions intégrées qui obligent votre programme à générer une erreur lorsque quelque chose ne va pas.

Cependant, vous devrez parfois créer des exceptions personnalisées répondant à vos besoins.

En Python, les utilisateurs peuvent définir de telles exceptions en créant une nouvelle classe. Cette classe d'exception doit être dérivée, directement ou indirectement, de la Exceptionclasse. La plupart des exceptions intégrées proviennent également de cette classe.

```python
>>> class CustomError(Exception):
...     pass
...

>>> raise CustomError
Traceback (most recent call last):
...
__main__.CustomError

>>> raise CustomError("An error occurred")
Traceback (most recent call last):
...
__main__.CustomError: An error occurred
```

Ici, nous avons créé une exception définie par l'utilisateur appelée CustomError dérivée de la classe `Exception`. Cette nouvelle exception peut être déclenchée, comme d'autres exceptions, en utilisant l' raiseinstruction avec un message d'erreur facultatif.

Lorsque nous développons un programme Python volumineux, il est recommandé de placer toutes les exceptions définies par l'utilisateur que notre programme génère dans un fichier distinct. De nombreux modules standard le font. Ils définissent leurs exceptions séparément exceptions.pyou errors.py(généralement mais pas toujours).

La classe des exceptions définies par l'utilisateur peut implémenter tout ce qu'une classe normale peut faire, mais nous les rendons généralement simples et concis. La plupart des implémentations déclarent une classe de base personnalisée et en déduisent d'autres classes d'exception. Ce concept est plus clair dans l'exemple suivant.

## Exception définie par l'utilisateur dans Python

Nous allons illustrer comment des exceptions définies par l'utilisateur peuvent être utilisées dans un programme pour générer et intercepter des erreurs.

Ce programme demandera à l'utilisateur d'entrer un numéro jusqu'à ce qu'il devine un numéro enregistré correctement. Pour les aider à comprendre, un indice est fourni, que leur estimation soit supérieure ou inférieure au nombre stocké.

```python
# define Python user-defined exceptions
class Error(Exception):
   """Base class for other exceptions"""
   pass

class ValueTooSmallError(Error):
   """Raised when the input value is too small"""
   pass

class ValueTooLargeError(Error):
   """Raised when the input value is too large"""
   pass

# our main program
# user guesses a number until he/she gets it right

# you need to guess this number
number = 10

while True:
   try:
       i_num = int(input("Enter a number: "))
       if i_num < number:
           raise ValueTooSmallError
       elif i_num > number:
           raise ValueTooLargeError
       break
   except ValueTooSmallError:
       print("This value is too small, try again!")
       print()
   except ValueTooLargeError:
       print("This value is too large, try again!")
       print()

print("Congratulations! You guessed it correctly.")
```

Voici un exemple d'utilisation de ce programme.

```
Enter a number: 12
This value is too large, try again!

Enter a number: 0
This value is too small, try again!

Enter a number: 8
This value is too small, try again!

Enter a number: 10
Congratulations! You guessed it correctly.
```

Ici, nous avons défini une classe de base appelée Error.

Les deux autres exceptions ( ValueTooSmallErroret ValueTooLargeError) qui sont réellement soulevées par notre programme proviennent de cette classe. C'est le moyen standard de définir des exceptions définies par l'utilisateur dans la programmation Python, mais vous n'êtes pas limité à cette seule manière.
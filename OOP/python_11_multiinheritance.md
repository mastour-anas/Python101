# Python Héritage Multiple

Comme `C++`, une classe peut être dérivée de plusieurs classes de base en Python. Cela s'appelle l'héritage multiple.

Dans l'héritage multiple, les caractéristiques de toutes les classes de base sont héritées dans la classe dérivée. La syntaxe de l'héritage multiple est similaire à l' héritage unique .

```Python
class Base1:
    pass

class Base2:
    pass

class MultiDerived(Base1, Base2):
    pass
```

Ici, `MultiDerived` est dérivé des classes `Base1` et `Base2` .

## Héritage à plusieurs niveaux en Python

D'autre part, nous pouvons également hériter d'une classe dérivée. Cela s'appelle l'héritage à plusieurs niveaux. Il peut être de n'importe quelle profondeur en Python.

Dans l'héritage à plusieurs niveaux, les fonctionnalités de la classe de base et de la classe dérivée sont héritées dans la nouvelle classe dérivée.

Un exemple avec visualisation correspondante est donné ci-dessous.

```Python
class Base:
    pass

class Derived1(Base):
    pass

class Derived2(Derived1):
    pass
```

Ici, `Derived1` est dérivé de Base et `Derived2` est dérivé de `Derived1` .

## Exemple : CalendarClock

Nous voulons introduire les principes de l'héritage multiple avec un exemple. Pour cela, nous allons implémenter des classes indépendantes: une classe `Clock` et une classe `Calendar`. Après cela, nous allons introduire une classe `CalendarClock`, qui, comme son nom l’indique, combine `Clock` et `Calendar`. `CalendarClock` hérite à la fois de `Clock` et `Calendar`.

La classe `Clock` simule le tic-tac d'une horloge. Une instance de cette classe contient l'heure, qui est stockée dans les attributs self.hours, self.minutes et self.seconds. Principalement, nous aurions pu écrire la méthode `__init__` et la méthode set comme ceci:

```Python
def __init__(self,hours=0, minutes=0, seconds=0):
    self._hours = hours
    self.__minutes = minutes
    self.__seconds = seconds

def set(self,hours, minutes, seconds=0):
    self._hours = hours
    self.__minutes = minutes
    self.__seconds = seconds
```

Nous nous sommes opposés à cette implémentation, car nous avons ajouté du code supplémentaire pour vérifier la plausibilité des données temporelles dans la méthode set. Nous appelons également la méthode set de la méthode `__init__`, car nous voulons contourner le code redondant.
La classe complète Clock:

```Python
class Clock(object):

    def __init__(self, hours, minutes, seconds):
        """
        The paramaters hours, minutes and seconds have to be 
        integers and must satisfy the following equations:
        0 <= h < 24
        0 <= m < 60
        0 <= s < 60
        """

        self.set_Clock(hours, minutes, seconds)

    def set_Clock(self, hours, minutes, seconds):
        """
        The parameters hours, minutes and seconds have to be 
        integers and must satisfy the following equations:
        0 <= h < 24
        0 <= m < 60
        0 <= s < 60
        """

        if type(hours) == int and 0 <= hours and hours < 24:
            self._hours = hours
        else:
            raise TypeError("Hours have to be integers between 0 and 23!")
        if type(minutes) == int and 0 <= minutes and minutes < 60:
            self.__minutes = minutes 
        else:
            raise TypeError("Minutes have to be integers between 0 and 59!")
        if type(seconds) == int and 0 <= seconds and seconds < 60:
            self.__seconds = seconds
        else:
            raise TypeError("Seconds have to be integers between 0 and 59!")

    def __str__(self):
        return "{0:02d}:{1:02d}:{2:02d}".format(self._hours,
                                                self.__minutes,
                                                self.__seconds)

    def tick(self):
        """
        This method lets the clock "tick", this means that the
        internal time will be advanced by one second.

        Examples:
        >>> x = Clock(12,59,59)
        >>> print(x)
        12:59:59
        >>> x.tick()
        >>> print(x)
        13:00:00
        >>> x.tick()
        >>> print(x)
        13:00:01
        """

        if self.__seconds == 59:
            self.__seconds = 0
            if self.__minutes == 59:
                self.__minutes = 0
                if self._hours == 23:
                    self._hours = 0
                else:
                    self._hours += 1
            else:
                self.__minutes += 1
        else:
            self.__seconds += 1


if __name__ == "__main__":
    x = Clock(23,59,59)
    print(x)
    x.tick()
    print(x)
    y = str(x)
    print(type(y))
```

Si vous appelez ce module seul, vous obtenez la sortie suivante:

```s
$ python3 clock.py
23:59:59
00:00:00
<classe 'str'>
```

Nous allons maintenant créer une classe `Calendar`, qui a beaucoup de similitudes avec la classe `Clock` précédemment définie. Au lieu de `tick`, nous avons une méthode `advance`, qui avance la date d'un jour, chaque fois qu'elle est appelée. Ajouter une journée à une date est assez compliqué. Nous devons vérifier si la date est le dernier jour d'un mois et si le nombre de jours dans les mois varie. Comme si cela ne suffisait pas, nous avons le problème de février et de l'année bissextile.

Les règles pour calculer une année bissextile sont les suivantes:
Si une année est divisible par 400, c'est une année bissextile.
Si une année n'est pas divisible par 400 mais par 100, ce n'est pas une année bissextile.
Un numéro d'année divisible par 4 mais pas par 100, c'est une année bissextile.
Tous les autres numéros de l'année sont des années courantes, c'est-à-dire sans années bissextiles.

```Python

class Calendar(object):

    months = (31,28,31,30,31,30,31,31,30,31,30,31)
    date_style = "British"

    @staticmethod
    def leapyear(year):
        """ 
        The method leapyear returns True if the parameter year
        is a leap year, False otherwise
        """
        if not year % 4 == 0:
            return False
        elif not year % 100 == 0:
            return True
        elif not year % 400 == 0:
            return False
        else:
            return True


    def __init__(self, d, m, y):
        """
        d, m, y have to be integer values and year has to be 
        a four digit year number
        """

        self.set_Calendar(d,m,y)


    def set_Calendar(self, d, m, y):
        """
        d, m, y have to be integer values and year has to be 
        a four digit year number
        """

        if type(d) == int and type(m) == int and type(y) == int:
            self.__days = d
            self.__months = m
            self.__years = y
        else:
            raise TypeError("d, m, y have to be integers!")


    def __str__(self):
        if Calendar.date_style == "British":
            return "{0:02d}/{1:02d}/{2:4d}".format(self.__days,
                                                   self.__months,
                                                   self.__years)
        else: 
            # assuming American style
            return "{0:02d}/{1:02d}/{2:4d}".format(self.__months,
                                                   self.__days,
                                                   self.__years)



    def advance(self):
        """
        This method advances to the next date.
        """

        max_days = Calendar.months[self.__months-1]
        if self.__months == 2 and Calendar.leapyear(self.__years):
            max_days += 1
        if self.__days == max_days:
            self.__days= 1
            if self.__months == 12:
                self.__months = 1
                self.__years += 1
            else:
                self.__months += 1
        else:
            self.__days += 1


if __name__ == "__main__":
    x = Calendar(31,12,2012)
    print(x, end=" ")
    x.advance()
    print("after applying advance: ", x)
    print("2012 was a leapyear:")
    x = Calendar(28,2,2012)
    print(x, end=" ")
    x.advance()
    print("after applying advance: ", x)
    x = Calendar(28,2,2013)
    print(x, end=" ")
    x.advance()
    print("after applying advance: ", x)
    print("1900 no leapyear: number divisible by 100 but not by 400: ")
    x = Calendar(28,2,1900)
    print(x, end=" ")
    x.advance()
    print("after applying advance: ", x)
    print("2000 was a leapyear, because number divisibe by 400: ")
    x = Calendar(28,2,2000)
    print(x, end=" ")
    x.advance()
    print("after applying advance: ", x)
    print("Switching to American date style: ")
    Calendar.date_style = "American"
    print("after applying advance: ", x)  
```


```
$ python3 calendar.py
31.12.2012 after applying advance:  01.01.2013
2012 was a leapyear:
28.02.2012 after applying advance:  29.02.2012
28.02.2013 after applying advance:  01.03.2013
1900 no leapyear: number divisible by 100 but not by 400: 
28.02.1900 after applying advance:  01.03.1900
2000 was a leapyear, because number divisibe by 400: 
28.02.2000 after applying advance:  29.02.2000
Switching to American date style: 
after applying advance:  02/29/2000
```

Enfin, nous en viendrons à notre exemple d'héritage multiple. Nous sommes maintenant capables d'implémenter la classe initialement prévue `CalendarClock`, qui héritera à la fois de l'horloge et du calendrier. La méthode `tick` de `Clock` devra être remplacée. Cependant, la nouvelle méthode `tick` de `CalendarClock` doit appeler la méthode `tick` de `Clock`: `Clock.tick(self)`

```Python
from clock import Clock
from calendar import Calendar


class CalendarClock(Clock, Calendar):
    """ 
        The class CalendarClock implements a clock with integrated 
        calendar. It's a case of multiple inheritance, as it inherits 
        both from Clock and Calendar      
    """

    def __init__(self,day, month, year, hour, minute, second):
        Clock.__init__(self,hour, minute, second)
        Calendar.__init__(self,day, month, year)


    def tick(self):
        """
        advance the clock by one second
        """
        previous_hour = self._hours
        Clock.tick(self)
        if (self._hours < previous_hour): 
            self.advance()

    def __str__(self):
        return Calendar.__str__(self) + ", " + Clock.__str__(self)


if __name__ == "__main__":
    x = CalendarClock(31,12,2013,23,59,59)
    print("One tick from ",x, end=" ")
    x.tick()
    print("to ", x)

    x = CalendarClock(28,2,1900,23,59,59)
    print("One tick from ",x, end=" ")
    x.tick()
    print("to ", x)

    x = CalendarClock(28,2,2000,23,59,59)
    print("One tick from ",x, end=" ")
    x.tick()
    print("to ", x)

    x = CalendarClock(7,2,2013,13,55,40)
    print("One tick from ",x, end=" ")
    x.tick()
    print("to ", x)
```

```
$ python3 calendar_clock.py 
One tick from  31/12/2013, 23:59:59 to  01/01/2014, 00:00:00
One tick from  28/02/1900, 23:59:59 to  01/03/1900, 00:00:00
One tick from  28/02/2000, 23:59:59 to  29/02/2000, 00:00:00
One tick from  07/02/2013, 13:55:40 to  07/02/2013, 13:55:41
```


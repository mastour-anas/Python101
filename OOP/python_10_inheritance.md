# Python Héritage

L'héritage est une fonctionnalité puissante dans la programmation orientée objet.

Il s'agit de définir une nouvelle classe avec peu ou pas de modification à une classe existante. La nouvelle classe est appelée classe dérivée (ou enfant) et celle dont elle hérite est appelée classe de base (ou parent) .

## Syntaxe d'héritage Python

```python
class BaseClass:
  Body of base class
class DerivedClass(BaseClass):
  Body of derived class
```

La classe dérivée hérite des fonctionnalités de la classe de base, y ajoutant de nouvelles fonctionnalités. Cela se traduit par une réutilisation du code.

## Exemple d'héritage en Python

Pour démontrer l'utilisation de l'héritage, prenons un exemple.

Un polygone est une figure fermée avec 3 côtés ou plus. Disons que nous avons une classe appelée Polygondéfinie comme suit.

```python
class Polygon:
    def __init__(self, no_of_sides):
        self.n = no_of_sides
        self.sides = [0 for i in range(no_of_sides)]

    def inputSides(self):
        self.sides = [float(input("Enter side "+str(i+1)+" : ")) for i in range(self.n)]

    def dispSides(self):
        for i in range(self.n):
            print("Side",i+1,"is",self.sides[i])
```

Cette classe a des attributs de données pour stocker le nombre de côtés, `n` et la magnitude de chaque côté en tant que liste, `sides` .

La méthode `inputSides()` prend l'ampleur de chaque côté et, de la même manière, `dispSides()` les affichera correctement.

Un triangle est un polygone à 3 côtés. Nous pouvons donc créer une classe appelée `Triangle` qui hérite de `Polygon`. Cela rend tous les attributs disponibles en classe `Polygon` facilement disponibles dans `Triangle`. Nous n'avons pas besoin de les définir à nouveau (réutilisabilité du code). `Triangle` est défini comme suit.

```python
class Triangle(Polygon):
    def __init__(self):
        Polygon.__init__(self,3)

    def findArea(self):
        a, b, c = self.sides
        # calculate the semi-perimeter
        s = (a + b + c) / 2
        area = (s*(s-a)*(s-b)*(s-c)) ** 0.5
        print('The area of the triangle is %0.2f' %area)
```

Cependant, la classe Triangle a une nouvelle méthode findArea() pour trouver et imprimer la zone du triangle. Voici un exemple.

```python
t = Triangle()

t.inputSides()
Enter side 1 : 3
fraichement diplomé
fraichement diplomé
fraichement diplomé
t.dispSides()
Side 1 is 3.0
Side 2 is 5.0
Side 3 is 4.0

t.findArea()
The area of the triangle is 6.00
```

Nous pouvons voir que, même si nous n’avons pas défini de méthodes telles que `inputSides()` ou `dispSides()` pour la classe `Triangle`, nous avons pu les utiliser.

Si un attribut est introuvable dans la classe, la recherche continue vers la classe de base. Cela se répète récursivement, si la classe de base est elle-même dérivée d'autres classes.

## Remplacement de méthode en Python

Dans l'exemple ci - dessus, notez que la méthode `__init__()` a été définie dans les deux classes, `Triangle` aussi bien `Polygon`. Lorsque cela se produit, la méthode de la classe dérivée remplace celle de la classe de base. C'est-à-dire `__init__()` en Triangle obtient la préférence sur le même dans `Polygon`.

Généralement, lors du remplacement d'une méthode de base, nous avons tendance à étendre la définition plutôt qu'à la remplacer. La même chose est faite en appelant la méthode dans la classe de base à partir de celle de la classe dérivée (appelant `Polygon.__init__()`depuis `__init__()` in `Triangle`).

Une meilleure option serait d'utiliser la fonction intégrée `super()`. Donc, `super().__init__(3)` est équivalent à `Polygon.__init__(self,3)` et est préféré.

Deux fonctions intégrées `isinstance()` et `issubclass()` sont utilisées pour vérifier les successions. Function `isinstance()` renvoie `True` si l'objet est une instance de la classe ou d'autres classes dérivées de celui-ci. Chaque classe de Python hérite de la classe de base object.

```python
isinstance(t,Triangle)
True

isinstance(t,Polygon)
True

isinstance(t,int)
False

isinstance(t,object)
True
```

De même, issubclass()est utilisé pour vérifier l'héritage de classe.

```python

issubclass(Polygon,Triangle)
False

issubclass(Triangle,Polygon)
True

issubclass(bool,int)
True
```
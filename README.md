# Python101

## Table of Contents

- Syntaxe du langage Python
  - [Syntaxe](Syntax/python_01:syntax.md)
  - [Block & Commentaire](Syntax/python_02:block_comment.md)
  - [Types de données](Syntax/python_03:data_type.md)
  - [Flux de controle](Syntax/python_04:flow_control.md)
  - [Fonction](Syntax/python_05:function.md)

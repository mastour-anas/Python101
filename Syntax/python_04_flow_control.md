# Contrôle de flux en Python

## Python if...else

La prise de décision est requise lorsque nous voulons exécuter un code uniquement si une certaine condition est remplie.

L'instruction if… elif… else est utilisée en Python pour la prise de décision.

### Syntaxe de if

```python
if test expression:
    statement(s)

```

Ici, le programme évalue l'expression de test et exécute les instructions uniquement si l'expression de texte est True.

Si l'expression du texte est `False`, les instructions ne sont pas exécutées.

Dans Python, le corps de l'instruction `if` est indiqué par l'indentation. Le corps commence par une indentation et la première ligne non induite marque la fin.

Python interprète les valeurs non nulles comme `True`. `None` et `0` sont interprétés comme `False`.

```python

# If the number is positive, we print an appropriate message

num = 3
if num > 0:
    print(num, "is a positive number.")
print("This is always printed.")

num = -1
if num > 0:
    print(num, "is a positive number.")
print("This is also always printed.")
```

### Syntaxe de if...else

```python
if test expression:
    Body of if
else:
    Body of else
```

L'instruction `if..else` évalue l'expression de test et exécute le corps de `if` uniquement lorsque la condition de test est True.

Si la condition est `False`, le corps d'autre est exécuté. L'indentation est utilisée pour séparer les blocs.

```python
# Program checks if the number is positive or negative
# And displays an appropriate message

num = 3

# Try these two variations as well. 
# num = -5
# num = 0

if num >= 0:
    print("Positive or Zero")
else:
    print("Negative number")
```

Dans l'exemple ci-dessus, lorsque `num` est égal à 3, l'expression de test est `True` et le corps de `if` est exécuté et le corps d'`else` est ignoré.

Si `num` est égal à -5, l'expression de test est fausse et le corps d'`else` est exécuté et le corps de `if` est ignoré.

Si `num` est égal à 0, l'expression de test est `True` et le corps de `if` est exécuté et le corps d'`else` est ignoré.

### Syntaxe de if...elif...else

```python
if test expression:
    Body of if
elif test expression:
    Body of elif
else: 
    Body of else
```

L'`elif` est court que `else if`. Cela nous permet de vérifier les expressions multiples.

Si la condition pour `if` est `False`, elle vérifie la condition du bloc `elif` suivant, etc.

Si toutes les conditions sont fausses, le corps d'autre est exécuté.

Un seul bloc parmi les plusieurs `if ... elif ..`. else est exécuté en fonction de la condition.

Le bloc `if` ne peut avoir qu'un seul bloc. Mais il peut avoir plusieurs blocs `elif`.

```python
# In this program, 
# we check if the number is positive or
# negative or zero and 
# display an appropriate message

num = 3.4

# Try these two variations as well:
# num = 0
# num = -4.5

if num > 0:
    print("Positive number")
elif num == 0:
    print("Zero")
else:
    print("Negative number")
```

Lorsque la variable `num` est positive, le nombre positif est imprimé.

Si `num` est égal à 0, le zéro est imprimé.

Si `num` est négatif, le nombre négatif est imprimé

## Python for Loop

Les boucles dans de Python sont utilisée pour parcourir une séquence (liste, tuple, chaîne) ou d’autres objets itérables.

```python
for val in sequence:
	Body of for

```

Ici, `val` est la variable qui prend la valeur de l'élément à l'intérieur de la séquence à chaque itération.

La boucle continue jusqu'à ce que nous atteignions le dernier élément de la séquence. Le corps de la boucle `for` est séparé du reste du code par une indentation.

```python
# Program to find the sum of all numbers stored in a list

# List of numbers
numbers = [6, 5, 3, 8, 4, 2, 5, 4, 11]

# variable to store the sum
sum = 0

# iterate over the list
for val in numbers:
	sum = sum+val

# Output: The sum is 48
print("The sum is", sum)
```

### range() function

Nous pouvons générer une séquence de nombres en utilisant la fonction `range()`. `range(10)` générera des nombres de 0 à 9 (10 chiffres).

Cette fonction ne stocke pas toutes les valeurs en mémoire, ce serait inefficace. Ainsi, il se souvient du début, de l’arrêt, de la taille de l’étape et génère le nombre suivant lors de ses déplacements.

Pour forcer cette fonction à sortir tous les éléments, nous pouvons utiliser la fonction `list()`.

L'exemple suivant clarifiera cela.

```python
# Output: range(0, 10)
print(range(10))

# Output: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print(list(range(10)))

# Output: [2, 3, 4, 5, 6, 7]
print(list(range(2, 8)))
```

### for loop avec else

Une boucle `for` peut également avoir un bloc `else` facultatif. La partie `else` est exécutée si les éléments de la séquence sont utilisés pour la boucle.

L'instruction `break` peut être utilisée pour arrêter une boucle `for`. Dans ce cas, la partie `else` est ignorée.

Par conséquent, une autre partie `for` `loop` s'exécute si aucune rupture ne se produit.

Voici un exemple pour illustrer cela.

```python
digits = [0, 1, 5]

for i in digits:
    print(i)
else:
    print("No items left.")
```

## Python while Loop

La boucle `while` en Python est utilisée pour parcourir un bloc de code tant que l'expression de `test(condition)` est vraie.

Nous utilisons généralement cette boucle lorsque nous ne connaissons pas à l'avance le nombre d'itérations à effectuer.

```python
while test_expression:
    Body of while
```

En boucle `while`, l'expression de test est vérifiée en premier. Le corps de la boucle est entré uniquement si l'`expression_test` est évaluée à `True`. Après une itération, l'expression de test est à nouveau vérifiée. Ce processus continue jusqu'à ce que `test_expression` soit évalué à `False`.

En Python, le corps de la boucle `while` est déterminé par l'indentation.

Le corps commence par une indentation et la première ligne non induite marque la fin.

Python interprète toute valeur non nulle comme `True`. `None` et `0` sont interprétés comme `False`.

```python
# Program to add natural
# numbers upto 
# sum = 1+2+3+...+n

# To take input from the user,
# n = int(input("Enter n: "))

n = 10

# initialize sum and counter
sum = 0
i = 1

while i <= n:
    sum = sum + i
    i = i+1    # update counter

# print the sum
print("The sum is", sum)

```

### while loop avec else

De même que pour for `loop`, nous pouvons également avoir un bloc facultatif `else` avec la boucle `while`.

La partie `else` est exécutée si la condition dans la boucle `while` a la valeur `False`.

La boucle `while` peut être terminée par une instruction `break`. Dans ce cas, la partie `else` est ignorée. Par conséquent, la partie `else` de la boucle `while` s'exécute si aucune rupture ne se produit et si la condition est fausse.

Voici un exemple pour illustrer cela.

```python
# Example to illustrate
# the use of else statement
# with the while loop

counter = 0

while counter < 3:
    print("Inside loop")
    counter = counter + 1
else:
    print("Inside else")
```

## Python break

L'instruction `break` termine la boucle le contenant. Le contrôle du programme passe à l'instruction immédiatement après le corps de la boucle.

Si l'instruction `break` se trouve dans une boucle imbriquée (boucle dans une autre boucle), `break` mettra fin à la boucle la plus interne.

```python
for val in "string":
    if val == "i":
        break
    print(val)

print("The end")
```

## Passage en Python

En programmation Python, `pass` est une instruction `null`. La différence entre un commentaire et une instruction de passage en Python est que, si l'interpréteur ignore entièrement un commentaire, le passage n'est pas ignoré.

Cependant, rien ne se produit lorsque pass est exécuté. Il en résulte aucune opération (NOP).

```python
# pass is just a placeholder for
# functionality to be added later.
sequence = {'p', 'a', 's', 's'}
for val in sequence:
    pass
```

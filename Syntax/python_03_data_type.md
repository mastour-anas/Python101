# Les variables et types

## Variable

Les variables ne sont rien que des emplacements de memoire sertvent a stocker  des valeurs. Cela  signifie que lorsque vous creer une variable, vous reservez un espace de memoire.

Python est complètement orienté objet, et non "statiquement typé". Vous n'avez pas besoin de déclarer des variables avant de les utiliser ou de déclarer leur type. Chaque variable dans Python est un objet et l'allocation de la memoire se fais automation quand vous affecter une valeur au variable.

```python
counter = 100          # An integer assignment
miles   = 1000.0       # A floating point
name    = "John"       # A string

print(counter)
print(miles)
print(name)
```

<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/EssentialKaleidoscopicBytecode?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

Python authorise l'assignement d'une valeur au plusieur variables simultane:

```python
a = b = c = 1
```

Ici, l'objet entier creer par la valeur `1`, les 3 variable sont assignees au meme espace de memoire, vous pouvez asigner plusieur object au plusieur variables simultanement:

```python
a, b, c = 1, 2, "john"
```

Ici, les 2 objets entier avec les valeurs `1` et `2` sont asignees au variable `a` et `b`, et l'object chaine decaractere avec la valeur `john` est assigner au variable `c`.

## Constante

Une constante est un type de variable dont la valeur ne peut pas être modifiée. Il est utile de considérer les constantes comme des conteneurs contenant des informations qui ne peuvent pas être modifiées ultérieurement.

Dans Python, les constantes sont généralement déclarées et utilisées sur le module, içi, le module signifie un nouveau fichier contienant des variables, des fonctions, etc., qui est importé dans le fichier principal. À l'intérieur du module, les constantes sont écrites en majuscules et les traits de soulignement séparant les mots.

```python
# constant.py
PI = 3.14
GRAVITY = 9.8
```

```python
# main.py
import constant

print(constant.PI)
print(constant.GRAVITY)
```

<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/UprightSparseCodec?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

Quelque exemples des constantes:
`False`, `True`, `None`


## Les types de données standard

Python possede plusieurs standard pour les types de donnees pour definir les operations possibles sur eux ainsi la methode de stockage pour chacun d'eux.
Python a plusieurs standards types de donnée:

| Object type | Example literals/creation|
|-------------|---------------------------|
| Numbers | 1234, 3.1415, 3+4j, 0b111, Decimal(), Fraction()|
| Strings | 'spam', "Bob's", b'a\x01c', u'sp\xc4m'|
| Lists | [1, [2, 'three'], 4.5], list(range(10))|
| Dictionaries | {'food': 'spam', 'taste': 'yum'}, dict(hours=10)|
| Tuples | (1, 'spam', 4, 'U'), tuple('spam'), namedtuple|
| Files | open('eggs.txt'), open(r'C:\ham.bin', 'wb')|
| Sets | set('abc'), {'a', 'b', 'c'}|
| Other | core types Booleans, types, None|

## Python numbers

Les types de données numériques stockent des valeurs numériques. Ce sont des types de données immuables (Un objet immuable, en programmation orientée objet et fonctionnelle, est un objet dont l'état ne peut pas être modifié après sa création. Ce concept est à contraster avec celui d'objet variable), ce qui signifie que la modification de la valeur d'un type de données numérique entraîne la création d'un nouvel objet.

Les objets numériques sont créés lorsque vous leur attribuez une valeur

```python
var1 = 1
var2 = 10
```

Python support plusieurs types numérique

- Int (entiers simple) - Ils sont souvent appelés simplement entiers ou ints, sont des nombres entiers positifs ou négatifs sans point décimal.

- Long (entiers longs) - Aussi appelés longs, ce sont des entiers de taille illimitée, écrits comme des entiers et suivis d'un `L`. majuscule ou minuscule.

- Float (valeurs réelles à virgule flottante) - Aussi appelés flotteurs, ils représentent des nombres réels et sont écrits avec un point décimal qui divise les parties entières et fractionnaires. Les flotteurs peuvent également être en notation scientifique, avec `E` ou `e` indiquant la puissance de 10 (2,5e2 = 2,5 x 102 = 250).

- Complex (nombres complexes) - sont de la forme `a + bJ`, où a et b sont des flottants et `J` (ou `j`) représente la racine carrée de -1 (qui est un nombre imaginaire). La partie réelle du nombre est `a` et la partie imaginaire est `b`. Les nombres complexes ne sont pas beaucoup utilisés en programmation Python.

Voici quelques exemples de nombres:

|int| long| float| complex|
----|-----|------|--------|
|10 |51924361L |0.0 |3.14j|
|100 |-0x19323L |15.20 |45.j|
|-786 |0122L |-21.9 |9.322e-36j|
|080 |0xDEFABCECBDAECBFBAEL |32.3+e18 |.876j|
|-0490 |535633629843L |-90. |-.6545+0J|
|-0x260 |-052318172735L |-32.54e100 |3e+26J|
|0x69 |-4721885298529L |70.2-E12 |4.53e-7j|

```python
a = 5

# Output: <class 'int'>
print(type(a))

# Output: <class 'float'>
print(type(5.0))

# Output: (8+3j)
c = 5 + 3j
print(c + 3)
```

<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/MildRustyAddon?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

### Les opérations mathématiques

#### Les opérations arithmétiques

|Opérateur |Déscription |Example|
|-----------|--------------|--------|
| `+` Addition | Ajoute des valeurs de chaque côté de l'opérateur. | `a + b = 30` |
| `-` Soustraction | Soustrait l'opérande de la main droite de l'opérande de gauche. | `a - b = -10` |
| `*` Multiplication | Multiplie les valeurs de chaque côté de l'opérateur | `a * b = 200` |
| `/` Division | Divise l'opérande gauche par l'opérande droit | `b / a = 2` |
| `%` Modulus | Divise l'opérande gauche par l'opérande droit et retourne le reste | `b % a = 0` |
| `**` Exponent | Effectue un calcul exponentiel (puissance) sur les opérateurs | `a ** b = 10 à la puissance 20` |
| `//` | Floor Division - La division des opérandes où le résultat est le quotient dans lequel les chiffres après le point décimal sont supprimés. Mais si l'un des opérandes est négatif, le résultat est paralysé, c'est-à-dire arrondi à zéro (vers l'infini négatif) - | `9 // 2 = 4` et ` 9.0 // 2.0 = 4.0`, `-11 // 3 = -4`, `-11,0 // 3 = -4,0` |

```python
#!/usr/bin/python

a = 21
b = 10
c = 0

c = a + b
print("Line 1 - Value of c is ", c)

c = a - b
print("Line 2 - Value of c is ", c )

c = a * b
print("Line 3 - Value of c is ", c )

c = a / b
print("Line 4 - Value of c is ", c) 

c = a % b
print("Line 5 - Value of c is ", c)

a = 2
b = 3
c = a**b 
print("Line 6 - Value of c is ", c)

a = 10
b = 5
c = a//b 
print("Line 7 - Value of c is ", c)
```
<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/SkinnyCoarseSystemsoftware?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

#### Les opération d'assignement

|Opérateur |Description |Example|
|-----------|--------------|--------|
| `=` | Assigne des valeurs des opérandes de droite à l'opérande de gauche | `c = a + b` assigne la valeur de `a + b` à `c` |
| `+ =` Add AND | Il ajoute l'opérande droit à l'opérande gauche et attribue le résultat à l'opérande gauche | `c + = a` est équivalent à `c = c + a` |
| `- =` Soustraire AND | Il soustrait l'opérande droit de l'opérande gauche et attribue le résultat à l'opérande gauche | `c - = a` est équivalent à `c = c - a` |
| `* =` Multiplier AND | Il multiplie l'opérande droit par l'opérande gauche et attribue le résultat à l'opérande gauche | `c * = a` est équivalent à `c = c * a` |
| `/ =` Diviser AND | Il divise l'opérande gauche par l'opérande droit et attribue le résultat à l'opérande gauche | `c / = a` est équivalent à `c = c / a` |
| `% =` Modulus AND | Il prend un module en utilisant deux opérandes et assigne le résultat à l'opérande gauche | c% = a est équivalent à c = c% a |
| `** =` Exponent AND | Exécute un calcul exponentiel (puissance) sur des opérateurs et attribue une valeur à l'opérande gauche | `c ** = a` est équivalent à` c = c ** a` |
| `// =` Floor Division | Il effectue la division des étages sur les opérateurs et attribue une valeur à l'opérande gauche | `c // = a` équivaut à `c = c // a` |



<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/KhakiInfatuatedGraphics?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

### Conversion de type numérique

Python convertit les nombres en interne dans une expression contenant des types mixtes vers un type commun pour évaluation. Mais parfois, vous devez forcer explicitement un nombre d'un type à un autre pour satisfaire aux exigences d'un paramètre d'opérateur ou de fonction.

- `int(x)` convertit `x` au entier simple.
- `long(x)` convertit `x` au entier long.
- `float(x)` convertit `x` au réel à virgule flottante.
- `complex(x)` convertit `x` au nombre complexe avec `x` comme réel.
- `complex(x,y)` convertit `x` et `y` au nombre complexe avec `x` comme réel et `y` comme nombre imaginaire.

### Fonctions mathématiques

| Fonction| Déscription |
|---------|-------------|
|`abs(x)`| la valeur absolue d'un nombre donnée|
|`ceil(x)`| le plus petit entier supérieur x|
|`cmp(x,y)`|  -1 si x<y , 0 si x == y ou 1 si x>y|
|`exp(x)`| exponentiel de x|
|`fabs(x)`| la valeur absolue d'un nombre donnée en réel|
|`floor(x)`| le plus grand entier inférieur a x|
|`log(x)`| logarithme de x, pour x>0|
|`log10(x)` | logarithme base 10 de x, pour x>0|
|`max(x1,x2,...)`|Le plus grand de ses arguments: la valeur la plus proche de l'infini positif|
|`min(x1,x2,...)`|Le plus petit de ses arguments: la valeur la plus proche du zero|
|`pow(x,y)`| x ** y |
|`sqrt(x)`| La racine carrée de x pour x> 0 |

### Constantes mathématiques

- `pi` la constante de `pi=3.14159265359`
- `e` la canstante de `e=2.71828182846`

## Python String

Les chaînes sont parmi les types les plus populaires en Python. Nous pouvons les créer simplement en entourant les caractères entre guillemets. Python considère les guillemets simples comme des guillemets. Créer des chaînes est aussi simple que d'attribuer une valeur à une variable. Par exemple -

```python
# all of the following are equivalent
my_string = 'Hello'
print(my_string)

my_string = "Hello"
print(my_string)

my_string = '''Hello'''
print(my_string)

# triple quotes string can extend multiple lines
my_string = """Hello, welcome to
           the world of Python"""
print(my_string)
```

<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/SlushyTrustingThings?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

### Manipulation des chaines de carractères

L'accés à des caractères individuels se fait en utilisant l'indexation du chaine de caractères et le tranchage. Index commence à partir de 0. l'accéder à un caractère hors de la plage d'index déclenche une erreur `IndexError`. L'index doit être un entier. Nous ne pouvons pas utiliser les types float ou autres, cela aboutira à `TypeError`.

Python authorise d'utilisation indexing négative pour ces séquences.

L'index de -1 fait référence au dernier élément, -2 au deuxième dernier élément, etc. Nous pouvons accéder à une gamme d'éléments dans une chaîne en utilisant l'opérateur de découpage (deux points).

|P|R|O|G|R|A|M|
|-|-|-|-|-|-|-|
|0|1|2|3|4|5|6|
|-7|-6|-5|-4|-3|-2|-1|

```python
str = 'program'
print('str = ', str)

#first character
print('str[0] = ', str[0])

#last character
print('str[-1] = ', str[-1])

#slicing 2nd to 5th character
print('str[1:5] = ', str[1:5])

#slicing 6th to 2nd last character
print('str[5:-2] = ', str[5:-2])
```

<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/FineTruthfulScreenscraper?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

### Modification ou la supression

Les chaînes de caractères sont immut ables. Cela signifie que les éléments d'une chaîne ne peuvent pas être modifiés une fois qu'ils ont été affectés. Nous pouvons simplement réaffecter différentes chaînes au même nom.

```python
>>> my_string = 'program'
>>> my_string[5] = 'a'
...
TypeError: 'str' object does not support item assignment
>>> my_string = 'Python'
>>> my_string
'Python'
```

Nous ne pouvons pas supprimer des caractères d'une chaîne. Mais la suppression complète de la chaîne est possible en utilisant `del`.

```python
>>> del my_string[1]
...
TypeError: 'str' object doesn't support item deletion
>>> del my_string
>>> my_string
...
NameError: name 'my_string' is not defined
```

### Les opérations spécial pour chaine de caractères

| Opérateur | Description | Exemple |
| --------------- | --------- | ------------ |
|`+`|Concaténation: l'ajout des valeurs de chaque côté de l'opérateur | `a + b` va donner` HelloPython` |
|`*`|Repetition: Crée de nouvelles chaînes, concaténant plusieurs copies de la même chaîne | `a * 2` donnera` HelloHello` |
|`[]`|Slice: Donne le caractère de l'index donné | `a [1]` donnera `e` |
|`[:]`|Range Slice: Donne les caractères de la plage donnée | `a [1: 4]` donnera `ell` |
|`in`|Membership : Retourne true si un caractère existe dans la chaîne donnée |`'H' in a` donnera `1` |
|`not in`| Membership: Retourne true si un caractère n'existe pas dans la chaîne donnée | `'M' not in a` donnera '1` |
|`%`| Format: Exécute le formatage de chaîne | Voir à la section suivante |

### Formatage des chaines de caractères

L'une des fonctionnalités les plus intéressantes de Python est l'opérateur de format de chaîne `%`. Cet opérateur est unique pour les chaînes et compense le paquet d'avoir des fonctions de la famille `printf()` de `C`. Voici un exemple simple

```python
# This prints out "Hello, John!"
name = "John"
print("Hello, %s!" % name)
```

<iframe height="400px" width="100%" src="https://repl.it/@tsubuta/AgonizingFickleDistributeddatabase?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

Voici la liste de l'ensemble complet des symboles pouvant être utilisés avec `%`

|Format Symbol |Conversion |
|--------------|-----------|
|`%c` |charactère|
| `%s` | conversion de chaîne via `str()` avant le formatage |
| `%i` | entier décimal signé |
| `%d` | entier décimal signé |
| `%u` | entier décimal non signé |
| `%o` | entier octal |
| `%x` | entier hexadécimal (lowercase letters)|
| `%X` | entier hexadécimal (UPPERcase letters) |
| `%e` | notation exponentielle (avec lowercase 'e') |
| `%E` | notation exponentielle (avec UPPERcase 'E') |
| `%f` | nombre réel à virgule flottante |

### Python format()

Python a introduit une nouvelle façon de formater les chaînes de caractères  Ce formatage de chaîne "nouveau style" supprime la syntaxe spéciale de `%` et rend la syntaxe du formatage des chaînes plus régulière. Le formatage est maintenant géré en appelant `.format()` sur un objet chaîne.

```python
>>> print 'Hello, {}'.format(name)
'Hello, Bob'
```

Avec un nouveau format de style, il est possible (et même obligatoire dans Python 2.6) de donner à des espaces réservés un index de position explicite.
Cela permet de réorganiser l'ordre d'affichage sans changer les arguments.

```python
print '{1} {0}'.format('one', 'two')
```


## Python List

Les listes sont
des collections d'objets positionnés de manière arbitraire et ordonnées, et elles n'ont pas de taille fixe.
Ils sont également mutables - contrairement aux chaînes, les listes peuvent être modifiées en place par affectation à
les décalages ainsi que divers appels de méthode de liste.

En Python, une liste est créée en plaçant tous les éléments dans un crochet [], séparés par des virgules.

```python
# empty list
my_list = []

# list of integers
my_list = [1, 2, 3]

# list with mixed datatypes
my_list = [1, "Hello", 3.4]
```

### access au element dans la liste

Nous pouvons utiliser l'opérateur d'index [] pour accéder à un élément dans une liste. Index commence à 0. Ainsi, une liste de 5 éléments aura un index de 0 à 4, sinon au sens inverse avec la negation comme dans les chaines de caracteres

|P|R|O|G|R|A|M|
|-|-|-|-|-|-|-|
|0|1|2|3|4|5|6|
|-7|-6|-5|-4|-3|-2|-1|

```python
list = ['physics', 'chemistry', 1997, 2000]
print "Value available at index 2 : "
print list[2]
list[2] = 2001
print "New value available at index 2 : "
print list[2]
```

### Extraction une liste d'éléments dans la liste

Nous pouvons accéder à une liste d'éléments dans une liste en utilisant l'opérateur de découpage (deux points).

```python
my_list = ['p','r','o','g','r','a','m']
# elements 3rd to 5th
print(my_list[2:5])

# elements beginning to 4th
print(my_list[:-5])

# elements 6th to end
print(my_list[5:])

# elements beginning to end
print(my_list[:])
```

### Modification et l'ajout des éléments dans la liste

Liste sont modifiables, ce qui signifie que leurs éléments peuvent être modifiés contrairement à la chaîne ou au tuple.

Nous pouvons utiliser l'opérateur d'affectation `=` pour modifier un élément ou une plage d'éléments.

```python
 mistake values
odd = [2, 4, 6, 8]

# change the 1st item
odd[0] = 1

# Output: [1, 4, 6, 8]
print(odd)

# change 2nd to 4th items
odd[1:4] = [3, 5, 7]  

# Output: [1, 3, 5, 7]
print(odd)
```

Nous pouvons ajouter un élément à une liste en utilisant la méthode `append()` ou ajouter plusieurs éléments en utilisant la méthode `extend()`.

```python
odd = [1, 3, 5]

odd.append(7)

# Output: [1, 3, 5, 7]
print(odd)

odd.extend([9, 11, 13])

# Output: [1, 3, 5, 7, 9, 11, 13]
print(odd)
```

Nous pouvons également utiliser `+` operator pour combiner deux listes. Ceci est aussi appelé concaténation.

L'opérateur `*` répète une liste pour le nombre de fois donné.

```python
odd = [1, 3, 5]

# Output: [1, 3, 5, 9, 7, 5]
print(odd + [9, 7, 5])

#Output: ["re", "re", "re"]
print(["re"] * 3)
```

### La suppression des éléments dans la liste

Nous pouvons supprimer un ou plusieurs éléments d'une liste en utilisant le mot-clé `del`. Il peut même supprimer la liste entièrement.

```pyhon
my_list = ['p','r','o','b','l','e','m']

# delete one item
del my_list[2]

# Output: ['p', 'r', 'b', 'l', 'e', 'm']
print(my_list)

# delete multiple items
del my_list[1:5]  

# Output: ['p', 'm']
print(my_list)

# delete entire list
del my_list

# Error: List not defined
print(my_list)
```

Nous pouvons utiliser la méthode `remove()` pour supprimer l’élément donné ou la méthode `pop()` pour supprimer un élément à l’index donné.

La méthode `pop()` supprime et retourne le dernier élément si l'index n'est pas fourni. Cela nous aide à implémenter des listes en tant que piles (structure de données premier entré, dernier sorti).

Nous pouvons également utiliser la méthode `clear()` pour vider une liste.


```python
my_list = ['p','r','o','b','l','e','m']
my_list.remove('p')

# Output: ['r', 'o', 'b', 'l', 'e', 'm']
print(my_list)

# Output: 'o'
print(my_list.pop(1))

# Output: ['r', 'b', 'l', 'e', 'm']
print(my_list)

# Output: 'm'
print(my_list.pop())

# Output: ['r', 'b', 'l', 'e']
print(my_list)

my_list.clear()

# Output: []
print(my_list)
```

### Liste des fonctions et methodes

|Fonction|Description|
|--------|------------|
|append() | Ajoute un élément à la fin de la liste|
|extend() | Ajoute tous les éléments d'une liste à une autre liste|
|insert() | Insère un élément à l'index défini|
|remove() | Supprime un élément de la liste|
|pop() | Supprime et retourne un élément à l'index donné|
|clear() | Supprime tous les éléments de la liste|
|index() | Retourne l'index du premier article correspondant|
|count() | Retourne le nombre d'éléments transmis en argument|
|sort() | Trier les éléments d'une liste par ordre croissant|
|reverse() | Inverse l'ordre des éléments dans la liste|
|copy() | Retourne une copie superficielle de la liste|
all() | Renvoie True si tous les éléments de la liste sont vrais (ou si la liste est vide).|
any() | Renvoie Vrai si un élément de la liste est vrai. Si la liste est vide, renvoyer False.|
enumerate() | Retourne un objet d'énumération. Il contient l'index et la valeur de tous les éléments de la liste sous la forme d'un tuple.|
len() | Renvoie la longueur (le nombre d'éléments) dans la liste.|
list () | Convertit un iterable (tuple, string, set, dictionary) en une liste.|
max() | Renvoie le plus gros article de la liste.|
min() | Renvoie le plus petit élément de la liste|
sorted() | Retourne une nouvelle liste triée (ne trie pas la liste elle-même).|
sum() | Renvoie la somme de tous les éléments de la liste.

```python
my_list = [3, 8, 1, 6, 0, 8, 4]

# Output: 1
print(my_list.index(8))

# Output: 2
print(my_list.count(8))

my_list.sort()

# Output: [0, 1, 3, 4, 6, 8, 8]
print(my_list)

my_list.reverse()

# Output: [8, 8, 6, 4, 3, 1, 0]
print(my_list)
```

## Python Tuple

En  Python, un tuple est similaire à list. La différence entre les deux est que nous ne pouvons pas modifier les éléments d’un tuple une fois qu’il est assigné alors que dans une liste, les éléments peuvent être modifiés.

### Avantages de Tuple over List

Puisque les tuples sont très similaires aux listes, ils sont tous deux utilisés dans des situations similaires.

Cependant, il y a certains avantages à implémenter un tuple qu'une liste. Voici quelques avantages principaux énumérés ci-dessous:

- Nous utilisons généralement tuple pour les types de données hétérogènes (différents) et listons les types de données homogènes (similaires).
- Puisque le tuple est immuable, il est plus rapide de parcourir le tuple qu'avec list. Il y a donc un léger gain de performance.
- Les tuples qui contiennent des éléments immuables peuvent être utilisés comme clé pour un dictionnaire. Avec la liste, ce n'est pas possible.
- Si vous avez des données qui ne changent pas, leur implémentation en tant que tuple garantira qu'elles restent protégées en écriture.

### Creation d'un tuple

Un tuple est créé en plaçant tous les éléments  entre parenthèses `()`, séparés par une virgule. Les parenthèses sont facultatives mais c'est une bonne pratique pour l'écrire.

Un tuple peut avoir n'importe quel nombre d'éléments et ils peuvent être de différents types (entier, flottant, liste, chaîne, etc.).

```python
# empty tuple
# Output: ()
my_tuple = ()
print(my_tuple)

# tuple having integers
# Output: (1, 2, 3)
my_tuple = (1, 2, 3)
print(my_tuple)

# tuple with mixed datatypes
# Output: (1, "Hello", 3.4)
my_tuple = (1, "Hello", 3.4)
print(my_tuple)

# nested tuple
# Output: ("mouse", [8, 4, 6], (1, 2, 3))
my_tuple = ("mouse", [8, 4, 6], (1, 2, 3))
print(my_tuple)

# tuple can be created without parentheses
# also called tuple packing
# Output: 3, 4.6, "dog"

my_tuple = 3, 4.6, "dog"
print(my_tuple)

# tuple unpacking is also possible
# Output:
# 3
# 4.6
# dog
a, b, c = my_tuple
print(a)
print(b)
print(c)
```

la création un tuple avec un élément est un peu compliqué.

Avoir un élément entre parenthèses ne suffit pas. Nous aurons besoin d'une virgule pour indiquer qu'il s'agit en fait d'un tuple.

```python
# only parentheses is not enough
# Output: <class 'str'>
my_tuple = ("hello")
print(type(my_tuple))

# need a comma at the end
# Output: <class 'tuple'>
my_tuple = ("hello",)  
print(type(my_tuple))

# parentheses is optional
# Output: <class 'tuple'>
my_tuple = "hello",
print(type(my_tuple))
```

### Accès aux éléments d'un tuple

Nous pouvons utiliser l'opérateur d'index `[]` pour accéder à un élément dans un tuple où l'index commence à 0.

Ainsi, un tuple ayant 6 éléments aura un index de 0 à 5. Essayer d'accéder à un élément autre que (6, 7, ...) déclenchera une erreur IndexError.

L'index doit être un entier, nous ne pouvons donc pas utiliser les types float ou autres. Cela se traduira par `TypeError`.

De même, le tuple imbriqué est accessible à l'aide de l'indexation imbriquée, comme illustré dans l'exemple ci-dessous.

```python
my_tuple = ('p','e','r','m','i','t')

# Output: 'p'
print(my_tuple[0])

# Output: 't'
print(my_tuple[5])

# index must be in range
# If you uncomment line 14,
# you will get an error.
# IndexError: list index out of range

#print(my_tuple[6])

# index must be an integer
# If you uncomment line 21,
# you will get an error.
# TypeError: list indices must be integers, not float

#my_tuple[2.0]

# nested tuple
n_tuple = ("mouse", [8, 4, 6], (1, 2, 3))

# nested index
# Output: 's'
print(n_tuple[0][3])

# nested index
# Output: 4
print(n_tuple[1][1])
```

#### Changer un tuple

Contrairement aux listes, les tuples sont immuables.

Cela signifie que les éléments d'un tuple ne peuvent pas être modifiés une fois qu'ils ont été affectés. Mais si l'élément est lui-même une liste de type de données mutable, ses éléments imbriqués peuvent être modifiés.

Nous pouvons également affecter un tuple à différentes valeurs (réaffectation).

```python
my_tuple = (4, 2, 3, [6, 5])

# we cannot change an element
# If you uncomment line 8
# you will get an error:
# TypeError: 'tuple' object does not support item assignment

#my_tuple[1] = 9

# but item of mutable element can be changed
# Output: (4, 2, 3, [9, 5])
my_tuple[3][0] = 9
print(my_tuple)

# tuples can be reassigned
# Output: ('p', 'r', 'o', 'g', 'r', 'a', 'm', 'i', 'z')
my_tuple = ('p','r','o','g','r','a','m','i','z')
print(my_tuple)
```

Nous pouvons utiliser `+` opérateur pour combiner deux tuples. Ceci est aussi appelé concaténation.

Nous pouvons également répéter les éléments dans un tuple pour un nombre de fois donné en utilisant l'opérateur `*`.

Les opérations `+` et `*` produisent toutes deux un nouveau tuple.

```python
# Concatenation
# Output: (1, 2, 3, 4, 5, 6)
print((1, 2, 3) + (4, 5, 6))

# Repeat
# Output: ('Repeat', 'Repeat', 'Repeat')
print(("Repeat",) * 3)
```

### Supprimer un tuple

Comme discuté ci-dessus, nous ne pouvons pas changer les éléments dans un tuple. Cela signifie également que nous ne pouvons pas supprimer ou supprimer des éléments d'un tuple.

Mais la suppression complète d’un tuple est possible avec le mot-clé `del`.

```python
my_tuple = ('p','r','o','g','r','a','m','i','z')

# can't delete items
# if you uncomment line 8,
# you will get an error:
# TypeError: 'tuple' object doesn't support item deletion

#del my_tuple[3]

# can delete entire tuple
# NameError: name 'my_tuple' is not defined
del my_tuple
my_tuple
```

### Liste des fonctions et methodes

| Fonction | Description |
| ------- | ------------ |
| count(x) | Renvoie le nombre d'éléments égal à x |
| index(x) | Retourne l'index du premier élément égal à x |
| all() | Return True si tous les éléments du tuple sont vrais (ou si le tuple est vide). |
| any() | Renvoie True si un élément du tuple est vrai. Si le tuple est vide, renvoyer False.
| enumerate () | Retourne un objet d'énumération. Il contient l'index et la valeur de tous les éléments de tuple sous forme de paires.
| len() | Renvoie la longueur (le nombre d'éléments) dans le tuple. |
| max() | Retourne le plus gros élément du tuple. |
| min() | Retourne le plus petit objet du tuple |
|sorted() | Prend des éléments dans le tuple et renvoie une nouvelle liste triée (ne trie pas le tuple lui-même). |
| sum() | Retourne la somme de tous les éléments du tuple. |
| tuple() | Convertit un iterable (liste, chaîne, set, dictionnaire) en un tuple. |

## Python Dictionary

Le dictionnaire Python est une collection non ordonnée d'éléments possèdant une paire clé: valeur.

Les dictionnaires sont optimisés pour récupérer des valeurs lorsque la clé est connue.

### Création d'un dictionnaire

Créer un dictionnaire est aussi simple que de placer des éléments entre accolades {} séparés par des virgules.

Un élément a une clé et la valeur correspondante exprimée en paire, clé: valeur.

Alors que les valeurs peuvent être de n'importe quel type de données et peuvent se répéter, les clés doivent être de type immuable (chaîne, nombre ou tuple avec des éléments immuables) et doivent être uniques.

```python
# empty dictionary
my_dict = {}

# dictionary with integer keys
my_dict = {1: 'apple', 2: 'ball'}

# dictionary with mixed keys
my_dict = {'name': 'John', 1: [2, 4, 3]}

# using dict()
my_dict = dict({1:'apple', 2:'ball'})

# from sequence having each item as a pair
my_dict = dict([(1,'apple'), (2,'ball')])

```

### Accès aux éléments d'un dictionaire

Le dictionnaire utilise des clés. La clé peut être utilisée entre crochets ou avec la méthode get ().

```python
my_dict = {'name':'Jack', 'age': 26}

# Output: Jack
print(my_dict['name'])

# Output: 26
print(my_dict.get('age'))

# Trying to access keys which doesn't exist throws error
# my_dict.get('address')
# my_dict['address']
```

### L'ajout et la modification d'un élément dans dictionnaire

Le dictionnaire est mutable. Nous pouvons ajouter de nouveaux éléments ou modifier la valeur des éléments existants à l'aide de l'opérateur d'affectation.

Si la clé est déjà présente, la valeur est mise à jour, sinon une nouvelle paire clé / valeur est ajoutée au dictionnaire.

```python

my_dict = {'name':'Jack', 'age': 26}

# update value
my_dict['age'] = 27

#Output: {'age': 27, 'name': 'Jack'}
print(my_dict)

# add item
my_dict['address'] = 'Downtown'  

# Output: {'address': 'Downtown', 'age': 27, 'name': 'Jack'}
print(my_dict)
```

### Supprimer un élément dans le dictionaire

Nous pouvons supprimer un élément particulier dans un dictionnaire en utilisant la méthode `pop()`. Cette méthode supprime en tant qu'élément la clé fournie et renvoie la valeur.

La méthode `popitem()` peut être utilisée pour supprimer et renvoyer un élément arbitraire (clé, valeur) du dictionnaire. Tous les éléments peuvent être supprimés à la fois en utilisant la méthode `clear()`.

Nous pouvons également utiliser le mot-clé del pour supprimer des éléments individuels ou le dictionnaire entier lui-même.

```python
# create a dictionary
squares = {1:1, 2:4, 3:9, 4:16, 5:25}  

# remove a particular item
# Output: 16
print(squares.pop(4))  

# Output: {1: 1, 2: 4, 3: 9, 5: 25}
print(squares)

# remove an arbitrary item
# Output: (1, 1)
print(squares.popitem())

# Output: {2: 4, 3: 9, 5: 25}
print(squares)

# delete a particular item
del squares[5]  

# Output: {2: 4, 3: 9}
print(squares)

# remove all items
squares.clear()

# Output: {}
print(squares)

# delete the dictionary itself
del squares

# Throws Error
# print(squares)
```

### Liste des fonctions et methodes

| Méthode | Description |
| ------- | ------------- |
| clear() | Supprimer tous les éléments du dictionnaire.
| copy() | Retourne une copie superficielle du dictionnaire. |
| fromkeys(seq [, v]) | Retourne un nouveau dictionnaire avec des clés de seq et une valeur égale à v (par défaut à Aucun). |
| get(clé [, d]) | Renvoie la valeur de la clé. Si la clé ne quitte pas, renvoyez d (par défaut à Aucun). |
| items() | Retourne une nouvelle vue des éléments du dictionnaire (clé, valeur). |
| key() | Retourne une nouvelle vue des clés du dictionnaire. |
| pop(touche [, d]) | Supprimez l'élément avec la clé et retournez sa valeur ou d si la clé est introuvable. Si d n'est pas fourni et que la clé est introuvable, déclenche KeyError. |
| popitem() | Supprimez et retournez un article arbitraire (clé, valeur). Relève KeyError si le dictionnaire est vide.
| setdefault(clé [, d]) | Si la clé est dans le dictionnaire, retournez sa valeur. Si ce n'est pas le cas, insérez la clé avec une valeur de d et renvoyez d (par défaut à Aucun). |
| update([autre]) | Mettez à jour le dictionnaire avec les paires clé / valeur des autres, en remplaçant les clés existantes.
| values​​() | Retourne une nouvelle vue des valeurs du dictionnaire |
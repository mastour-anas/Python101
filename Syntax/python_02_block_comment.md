# Les blocs, les commentaires

## Structuration et notion de bloc

En python, chaque instruction s'écrit sur une ligne sans mettre d'espace:

```python
a = 20
b = 15
print(a, b)
```

Les instructions simples peuvent cependant être mises sur la même ligne en les separant par des points virgules `;`, les lignes étant executées dans l'ordre de gauche a droite:

```python
a = 20; b = 15; print(a, b)
```

La séparation entre les en tetes qui sont ligne de definition de boucle, fonction ou classe et le contenus (bloc) d'instruction correspendant se fais par 'indentation' de lignes, une identation se fait par `4 espace` ou bien `tabulation`:

```python
a = 3
if a is not None:
    print(a)
else:
    print('Hello world')
print('lorem ipsum')
```

## Lignes trop longues

Il peut arriver que les expressions sur une ligne deviennent trop longue pour une bonne lisibilité du code ou tout simplement pour améliorer la lisibilité du code. Il est alors possible de poursuivre les instructions en utilisant le symbole '\' (le symbole '\n' signifie retour à la ligne):

```python
labamba = "Para bailar la bamba,\n" \
          "Para bailar la bamba,\n" \
          "Se necesita una poca de gracia.\n" \
          "Una poca de gracia pa mi pa ti."
print(labamba)

from math import exp, sin, log
a = lambda x: exp( - sin( x) / x ) * log(x + 1) \
              + sin(x) / exp(x) \
              - 2
```

Pour le text, ces deux solution sont possible:

```python
labamba = "Para bailar la bamba,\n\
Para bailar la bamba,\n\
Se necesita una poca de gracia.\n\
Una poca de gracia pa mi pa ti."

labamba = """Para bailar la bamba,
Para bailar la bamba,
Se necesita una poca de gracia.
Una poca de gracia pa mi pa ti."""
```

pour les fonctions, la poursuite du code doit etre distingue d'indentation :

```python
def long_function_name(var_one, var_two,
                       var_three, var-four):


def long_function_name(
        var_one, var_two, var_three,
        var_four):
```

## Commentaires

Les commentaires sont des lignes qui existent dans les programmes d'ordinateur qui sont ignorés par les compilateurs et les interpretes. Inclusion des commentaires dans les programmes rend le code plus lisible pour les humains car il fournit des informations ou des explications sur ce que fait chaque partie d'un programme.

Un commentaire commence par le symbole `#` avec un espace apres et continue jusqua la fin de la ligne :

```python
# this is the first comment
spam = 1  # and this is the second comment
          # ... and now a third!
text = "# This is not a comment because it's inside quotes."
```

Les commentaires qui contient plusieurs lignes, utilises pour exipliquer les choses en detail, sont crees en ajoutant un delimiteur `"` a chaque extremite du text

```python
"""
The main function will parse arguments via the parser variable.  These
arguments will be defined by the user on the console.  This will pass
the word argument the user wants to parse along with the filename the
user wants to use, and also provide help text if the user does not correctly pass the arguments.
"""
```

### Conseils PEP

- Les commentaires qui contredisent le code sont pires que pas de commentaires. Toujours faire une priorité de garder les commentaires à jour lorsque le code change!

- Les commentaires doivent être des phrases complètes. Le premier mot doit être en majuscule, sauf s'il s'agit d'un identifiant commençant par une lettre minuscule (ne jamais modifier le cas des identifiants!).

- Les commentaires de bloc consistent généralement en un ou plusieurs paragraphes construits à partir de phrases complètes, chaque phrase se terminant par une période.

- Lorsque vous écrivez en anglais, suivez Strunk & White.

- Codeurs Python de pays non anglophones: écrivez vos commentaires en anglais, sauf si vous êtes sûr à 120% que le code ne sera jamais lu par des personnes qui ne parlent pas votre langue.

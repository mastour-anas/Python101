# Python File I/O

Le fichier est un emplacement nommé sur le disque pour stocker les informations associées. Il est utilisé pour stocker de manière permanente des données dans une mémoire non volatile (par exemple un disque dur).

Depuis, la mémoire vive (RAM) est volatile, ce qui perd ses données lorsque l'ordinateur est éteint, nous utilisons des fichiers pour une utilisation future des données.

Lorsque nous voulons lire ou écrire dans un fichier, nous devons l’ouvrir en premier. Lorsque nous avons terminé, il doit être fermé pour que les ressources liées au fichier soient libérées.

Ainsi, en Python, une opération de fichier a lieu dans l'ordre suivant.

- Ouvrir un fichier
- Lire ou écrire (effectuer une opération)
- Ferme le fichier

## Ouvrir un fichier

Python a une fonction intégrée `open()` pour ouvrir un fichier. Cette fonction renvoie un objet fichier, également appelé handle, car il est utilisé pour lire ou modifier le fichier en conséquence.

```python
f = open("test.txt")    # open file in current directory
f = open("C:/Python33/README.txt")  # specifying full path
```

Nous pouvons spécifier le mode lors de l'ouverture d'un fichier. En mode, nous spécifions si nous voulons lire 'r', écrire 'w'ou ajouter 'a'au fichier. Nous spécifions également si nous voulons ouvrir le fichier en mode texte ou en mode binaire.

La valeur par défaut est la lecture en mode texte. Dans ce mode, nous obtenons des chaînes lors de la lecture du fichier.

D'autre part, le mode binaire renvoie des octets et c'est le mode à utiliser pour traiter des fichiers non textuels tels que les fichiers image ou exe.

|Mode |La description|
|-----|--------------|
|'r' |Ouvrez un fichier pour la lecture. (défaut)|
|'w' |Ouvrez un fichier pour l'écriture. Crée un nouveau fichier s'il n'existe pas ou tronque le fichier s'il existe.|
|'X' |Ouvrez un fichier pour la création exclusive. Si le fichier existe déjà, l'opération échoue.|
|'a' |Ouvrir pour ajouter à la fin du fichier sans le tronquer. Crée un nouveau fichier s'il n'existe pas.|
|'t' |Ouvrir en mode texte. (défaut)|
|'b' |Ouvrir en mode binaire.|
|'+' |Ouvrir un fichier pour la mise à jour (lecture et écriture)|

```python
f = open("test.txt")      # equivalent to 'r' or 'rt'
f = open("test.txt",'w')  # write in text mode
f = open("img.bmp",'r+b') # read and write in binary mode
```

Lorsque vous travaillez avec des fichiers en mode texte, il est fortement recommandé de spécifier le type de codage.

```python
f = open("test.txt",mode = 'r',encoding = 'utf-8')
```

## Fermeture fichier

La fermeture d'un fichier libère les ressources liées au fichier et se fait en utilisant la méthode `close()`  .

Python a un ramasse-miettes pour nettoyer les objets non référencés, mais nous ne devons pas compter sur lui pour fermer le fichier.

```python
f = open("test.txt",encoding = 'utf-8')
# perform file operations
f.close()
```

Cette méthode n'est pas entièrement sûre. Si une exception se produit lorsque nous effectuons une opération avec le fichier, le code se ferme sans fermer le fichier.

Un moyen plus sûr consiste à utiliser un bloc `try ... finally` .

```python
try:
   f = open("test.txt",encoding = 'utf-8')
   # perform file operations
finally:
   f.close()
```

De cette façon, nous sommes assurés que le fichier est correctement fermé, même si une exception est déclenchée, ce qui provoque l'arrêt du flux de programme.

La meilleure façon de procéder consiste à utiliser l'instruction `with`. Cela garantit que le fichier est fermé lorsque le bloc à l'intérieur withest quitté.

Nous n'avons pas besoin d'appeler explicitement la méthode `close()`. C'est fait en interne.

```python
with open("test.txt",encoding = 'utf-8') as f:
   # perform file operations
```

## Ecriture dans un fichier

Pour écrire dans un fichier en Python, nous devons l'ouvrir en écriture 'w', en append 'a'ou en 'x' mode de création exclusif .

Nous devons faire attention au 'w' mode car il va écraser dans le fichier s'il existe déjà. Toutes les données précédentes sont effacées.

L'écriture d'une chaîne ou d'une séquence d'octets (pour les fichiers binaires) s'effectue à l'aide de la méthode `write()`. Cette méthode renvoie le nombre de caractères écrits dans le fichier.

```python
with open("test.txt",'w',encoding = 'utf-8') as f:
   f.write("my first file\n")
   f.write("This file\n\n")
   f.write("contains three lines\n")
```

Ce programme créera un nouveau fichier nommé `test.txt` s'il n'existe pas. S'il existe, il est écrasé.

Nous devons inclure les caractères de la nouvelle ligne pour distinguer différentes lignes.

Comment lire des fichiers en Python?
Pour lire un fichier en Python, il faut ouvrir le fichier en mode lecture.

Il existe différentes méthodes disponibles à cet effet. Nous pouvons utiliser la méthode `read(size)` pour lire le nombre de tailles de données. Si le paramètre de taille n'est pas spécifié, il lit et retourne à la fin du fichier.

```python
>>> f = open("test.txt",'r',encoding = 'utf-8')
>>> f.read(4)    # read the first 4 data
'This'

>>> f.read(4)    # read the next 4 data
' is '

>>> f.read()     # read in the rest till end of file
'my first file\nThis file\ncontains three lines\n'

>>> f.read()  # further reading returns empty sting
''
```

Nous pouvons le voir, la méthode `read()` renvoie nouvel ligne comme '\n'. Une fois la fin du fichier atteinte, nous obtenons une chaîne vide lors de la lecture ultérieure.

Nous pouvons changer notre curseur de fichier actuel (position) en utilisant la méthode `seek()` De même, la méthode `tell()` renvoie notre position actuelle (en nombre d'octets).

```python
>>> f.tell()    # get the current file position
56

>>> f.seek(0)   # bring file cursor to initial position
0

>>> print(f.read())  # read the entire file
This is my first file
This file
contains three lines
```

Nous pouvons lire un fichier ligne par ligne en utilisant une boucle `for` . C'est à la fois efficace et rapide.

```python
>>> for line in f:
...     print(line, end = '')
...
This is my first file
This file
contains three lines
```

Les lignes dans le fichier lui-même ont un caractère de nouvelle ligne `\n`.

Alternativement, nous pouvons utiliser la méthode `readline()` pour lire des lignes individuelles d'un fichier. Cette méthode lit un fichier jusqu'à la nouvelle ligne, y compris le caractère de nouvelle ligne.

```python
>>> f.readline()
'This is my first file\n'

>>> f.readline()
'This file\n'

>>> f.readline()
'contains three lines\n'

>>> f.readline()
''
```

Enfin, la méthode `readlines()` renvoie une liste des lignes restantes du fichier entier. Toutes ces méthodes de lecture renvoient des valeurs vides lorsque la fin du fichier (EOF) est atteinte.

```python
f.readlines()
['This is my first file\n', 'This file\n', 'contains three lines\n']
```

## Méthodes de fichiers Python

Il existe différentes méthodes disponibles avec l'objet fichier. Certains d'entre eux ont été utilisés dans les exemples ci-dessus.

Voici la liste complète des méthodes en mode texte avec une brève description.

|Méthode | Description|
|--------|------------|
|close() | Fermez un fichier ouvert. Cela n'a aucun effet si le fichier est déjà fermé.|
|fileno() | Retourne un nombre entier (descripteur de fichier) du fichier.|
|flush() | Videz le tampon d'écriture du flux de fichiers.|
|isatty() | Renvoie `True` si le flux de fichiers est interactif.|
|read(n) | Lire au plus n caractères forme le fichier. Lit jusqu'à la fin du fichier s'il est négatif ou None.|
|readable() | Retourne `True` si le flux de fichiers peut être lu.|
|readline(n=-1) | Lisez et renvoyez une ligne du fichier. Lit au maximum n octets si spécifié.|
|readlines(n=-1) | Lisez et retournez une liste de lignes du fichier. Lit au maximum n octets/caractères si spécifié.|
|seek(offset,from = SEEK_SET) | Modifiez la position du fichier pour décaler les octets, en référence à from (début, courant, fin).|
|seekable() | Retourne `True` si le flux de fichiers prend en charge l'accès aléatoire.|
|tell() | Renvoie l'emplacement du fichier en cours.|
|tronquer(size = None) | Redimensionnez le flux de fichiers pour dimensionner les octets. Si la taille n'est pas spécifiée, redimensionnez à l'emplacement actuel.|
|accessible en écriture () | Renvoie `True` si le flux de fichiers peut être écrit.|
|write(s) | Écrivez la chaîne s dans le fichier et renvoyez le nombre de caractères écrits.|
|writelines(lines) | Écrivez une liste de lignes dans le fichier.|
# les fonctions dans python

En Python, la fonction est un groupe d'instructions liées qui effectuent une tâche spécifique.

Les fonctions aident à diviser notre programme en morceaux plus petits et modulaires. À mesure que notre programme grandit, les fonctions le rendent plus organisé et plus facile à gérer.

De plus, cela évite la répétition et rend le code réutilisable.

```python
def function_name(parameters):
    """docstring"""
    statement(s)
```

Ci-dessus, une définition de fonction est constituée des composants suivants.

- Le mot-clé `def` marque le début de l'en-tête de fonction.
- Un nom de fonction pour l'identifier de manière unique. Le nommage des fonctions suit les mêmes règles d'écriture des identifiants en Python.
- Paramètres `(arguments)` par lesquels nous transmettons des valeurs à une fonction. Ils sont optionnels.
- Deux points (`:`) pour marquer l'en-tête de fin de fonction.
- Chaîne de documentation optionnelle (docstring) pour décrire ce que fait la fonction.
- Une ou plusieurs instructions python valides constituant le corps de la fonction. Les instructions doivent avoir le même niveau d'indentation (généralement 4 espaces).
- Une instruction de `return` facultative pour renvoyer une valeur de la fonction.

```python
def greet(name):
    """This function greets to
    the person passed in as
    parameter"""
    print("Hello, " + name + ". Good morning!")
```

Une fois que nous avons défini une fonction, nous pouvons l'appeler à partir d'une autre fonction, d'un programme ou même de l'invite Python. Pour appeler une fonction, il suffit de taper le nom de la fonction avec les paramètres appropriés.

```python
greet("paul")
# Hello, Paul. Good morning!
```

## Doctrings

La première chaîne après l'en-tête de fonction est appelée `docstring` et est l'abréviation de chaîne de documentation. Il est utilisé pour expliquer brièvement ce que fait une fonction.

Bien que facultative, la documentation est une bonne pratique de programmation. Si vous ne vous souvenez pas de ce que vous avez mangé la semaine dernière, documentez toujours votre code.

Dans l'exemple ci-dessus, nous avons un `docstring` immédiatement sous l'en-tête de la fonction. Nous utilisons généralement des guillemets triples pour que `docstring` puisse s'étendre sur plusieurs lignes. Cette chaîne est disponible pour nous en tant qu'attribut `__doc__` de la fonction.

Par exemple:

Essayez d'exécuter ce qui suit dans le shell Python pour voir la sortie.

```python
>>> print(greet.__doc__)
This function greets to
	the person passed into the
	name parameter
```

## Les arguments

Vous pouvez appeler une fonction en utilisant les types suivants d'arguments formels:

- Arguments requis
- Arguments de mots clés
- Arguments par défaut
- Arguments Arbitraire

### Arguments requis

Les arguments requis sont les arguments transmis à une fonction dans un ordre positionnel correct. Ici, le nombre d'arguments dans l'appel de fonction doit correspondre exactement à la définition de la fonction.

Pour appeler la fonction `greet()`, vous devez absolument passer un argument, sinon une erreur de syntaxe se produit comme suit

```python
def greet(name):
    """This function greets to
    the person passed in as
    parameter"""
    print("Hello, " + name + ". Good morning!")

greet()
```

Lorsque le code ci-dessus est exécuté, il produit le résultat suivant

```
Traceback (most recent call last):
   File "test.py", line 11, in <module>
      greet();
TypeError: greet() takes exactly 1 argument (0 given)
```

### Arguments de mots clés

Les arguments de mot clé sont liés aux appels de fonction. Lorsque vous utilisez des arguments de mot-clé dans un appel de fonction, l'appelant identifie les arguments par le nom du paramètre.

Cela vous permet d'ignorer des arguments ou de les mettre en désordre car l'interpréteur Python peut utiliser les mots-clés fournis pour faire correspondre les valeurs avec les paramètres. Vous pouvez également effectuer des appels de mots-clés à la fonction greet() de la manière suivante:

```python
def greet(name):
    """This function greets to
    the person passed in as
    parameter"""
    print("Hello, " + name + ". Good morning!")

greet(name= "paul")

>>> Hello, Paul. Good morning!
```

### Arguments par défaut

Un argument par défaut est un argument qui suppose une valeur par défaut si aucune valeur n'est fournie dans l'appel de fonction pour cet argument. L'exemple suivant donne une idée sur les arguments par défaut, il imprime le message par défaut s'il n'est pas passé.

```python
# Function definition is here
def greet(name, msg = "Good morning!"):
   """
   This function greets to
   the person with the
   provided message.

   If message is not provided,
   it defaults to "Good
   morning!"
   """

   print("Hello",name + ', ' + msg)

greet("Kate")
greet("Bruce","How do you do?")
```

Lorsque le code ci-dessus est exécuté, il produit le résultat suivant

```
Hello Kate, Good morning!
Hello Bruce, How do you do?
```

### Arguments Arbitraire

Parfois, nous ne savons pas à l’avance le nombre d’arguments qui seront passés dans une fonction. Python nous permet de gérer ce genre de situation via des appels de fonctions avec un nombre arbitraire d’arguments.

Dans la définition de la fonction, nous utilisons un astérisque (*) avant le nom du paramètre pour indiquer ce type d'argument. Voici un exemple.

```python
def greet(*names):
   """This function greets all
   the person in the names tuple."""

   # names is a tuple with arguments
   for name in names:
       print("Hello",name)

greet("Monica","Luke","Steve","John")
```

```
Hello Monica
Hello Luke
Hello Steve
Hello John
```

## Récursivité

Nous savons qu'en Python, une fonction peut appeler d'autres fonctions. Il est même possible que la fonction s'appelle elle-même. Ce type de construction est appelé fonctions récursives.

Voici un exemple de fonction récursive pour trouver la factorielle d'un entier.

Factorial d'un nombre est le produit de tous les entiers de 1 à ce nombre. Par exemple, la factorielle de 6 (notée 6!) Est 1 * 2 * 3 * 4 * 5 * 6 = 720.

```python
# An example of a recursive function to
# find the factorial of a number

def calc_factorial(x):
    """This is a recursive function
    to find the factorial of an integer"""

    if x == 1:
        return 1
    else:
        return (x * calc_factorial(x-1))

num = 4
print("The factorial of", num, "is", calc_factorial(num))
```

Dans l'exemple ci-dessus, calc_factorial()une fonction récursive s'appelle elle-même.

Lorsque nous appelons cette fonction avec un entier positif, elle s'appellera récursivement en diminuant le nombre.

Chaque appel de fonction multiplie le nombre par la factorielle du nombre 1 jusqu'à ce que le nombre soit égal à un. Cet appel récursif peut être expliqué dans les étapes suivantes.

```
calc_factorial(4)              # 1st call with 4
4 * calc_factorial(3)          # 2nd call with 3
4 * 3 * calc_factorial(2)      # 3rd call with 2
4 * 3 * 2 * calc_factorial(1)  # 4th call with 1
4 * 3 * 2 * 1                  # return from 4th call as number=1
4 * 3 * 2                      # return from 3rd call
4 * 6                          # return from 2nd call
24                             # return from 1st call
```

### Avantages de la récursivité

- Les fonctions récursives rendent le code propre et élégant.
- Une tâche complexe peut être décomposée en sous-problèmes plus simples en utilisant la récursivité.
- La génération de séquence est plus facile avec la récursivité qu'avec une itération imbriquée.

### Inconvénients de la récursivité

- Parfois, la logique de la récursivité est difficile à suivre.
- Les appels récursifs sont coûteux (inefficaces) car ils nécessitent beaucoup de mémoire et de temps.
- Les fonctions récursives sont difficiles à déboguer.

## Fonction Python Anonyme / Lambda

En Python, la fonction anonyme est une fonction définie sans nom.
Alors que les fonctions normales sont définies à l'aide du mot-clé `def`, les fonctions anonymes sont définies à l'aide du mot-clé `lambda` dans Python .
Par conséquent, les fonctions anonymes sont également appelées fonctions `lambda`.

Une fonction lambda en python a la syntaxe suivante.

```python
lambda arguments: expression
```

Les fonctions Lambda peuvent avoir un nombre quelconque d'arguments, mais une seule expression. L'expression est évaluée et renvoyée. Les fonctions Lambda peuvent être utilisées partout où des objets de fonction sont requis.

```python
# Program to show the use of lambda functions

double = lambda x: x * 2

# Output: 10
print(double(5))
```

Dans le programme ci-dessus, `lambda x: x * 2` est une fonction lambda. Ici, `x` est l'argument et `x * 2` est l'expression qui est évaluée et renvoyée.

Cette fonction n'a pas de nom. Il retourne un objet fonction affecté à l'identifiant `double`. Nous pouvons maintenant l'appeler comme une fonction normale. La déclaration

```python
double = lambda x: x * 2
```

est presque même que

```python
def  double(x):
    return x * 2
```

### map
L'avantage de l'opérateur lambda peut être vu lorsqu'il est utilisé avec la fonction `map()`.

```python
r = map (func, seq)
```

Le premier argument `func` est le nom d'une fonction et le second une séquence `seq` (par exemple une liste)  . `map()` applique la fonction `func` à tous les éléments de la séquence `seq` . Il retourne une nouvelle liste avec les éléments modifiés par `func`

```python
def fahrenheit(T):
    return ((float(9)/5)*T + 32)
def celsius(T):
    return (float(5)/9)*(T-32)
temp = (36.5, 37, 37.5,39)

F = map(fahrenheit, temp)
C = map(celsius, F)
```

Dans l'exemple ci-dessus, nous n'avons pas utilisé `lambda`. En utilisant `lambda`, nous n'aurions pas dû définir et nommer les fonctions `fahrenheit()` et `celsius()`. Vous pouvez le voir dans la session interactive suivante:

```python
>>> Celsius = [39.2, 36.5, 37.3, 37.8]
>>> Fahrenheit = map (lambda x: (float (9) / 5) * x + 32, Celsius)
>>> imprimer Fahrenheit
[102.56, 97.700000000000003, 99.140000000000001, 100.03999999999999]
>>> C = map (lambda x: (float (5) / 9) * (x-32), Fahrenheit)
>>> print C
[39.200000000000003, 36.5, 37.300000000000004, 37.799999999999997]
>>>
```

### filter

La fonction `filter(function, list)` offre un moyen élégant de filtrer tous les éléments d’une liste pour lesquels la fonction retourne `True`.
La fonction `filter(f, l)` a besoin d'une fonction `f` comme premier argument. `f` renvoie une valeur booléenne, soit `True` ou `False`. Cette fonction sera appliquée à tous les éléments de la liste `l` . Seulement si `f` renvoie `True`, l'élément de la liste sera inclus dans la liste des résultats.

```python
>>> fib = [0,1,1,2,3,5,8,13,21,34,55]
>>> result = filter(lambda x: x % 2, fib)
>>> print result
[1, 1, 3, 5, 13, 21, 55]
>>> result = filter(lambda x: x % 2 == 0, fib)
>>> print result
[0, 2, 8, 34]
>>>
```

### reduce

La fonction `reduce(func, seq)` applique continuellement la fonction `func()` à la séquence `seq`. Il renvoie une valeur unique.
Si `seq = [s1 , s2 , s3 , ..., sn ]`, l'appel de `reduce(func, seq)` fonctionne comme ceci:
Au début, les deux premiers éléments de seq seront appliqués à func, c-à-d. `Func(s1 , s2 )`. La liste sur laquelle fonctionne `Reduce()` ressemble maintenant à: `func(s1 , s2 ), s3 , .., sn ]`
Dans la prochaine étape, func sera appliqué au résultat précédent et au troisième élément de la liste, à savoir `func(func (s1 , s2 ), s3 )`.
La liste ressemble à ceci: `[func(func (s1 , s2 ), s3 ), ..., sn ]`
Continuez comme ceci jusqu'à ce qu'il ne reste qu'un seul élément et renvoyez cet élément comme résultat de `Reduce()`
Nous illustrons ce processus dans l'exemple suivant

`>>> reduce(lambda x, y: x + y, [47,11,42,13])
113`

![alt text] (https://www.python-course.eu/images/reduce_diagram.png "expliquation")

Déterminer le maximum d'une liste de valeurs numériques en utilisant reduce:

```python
>>> f = lambda a, b: a si (a> b) sinon b
>>> reduce(f, [47,11,42,102,13])
102
>>>
```

Calcul de la somme des nombres de 1 à 100:

```python
>>> reduce(lambda x, y: x + y, gamme (1,101))
5050
```

## Variables globales

En Python, une variable déclarée en dehors de la fonction ou dans la portée globale est appelée variable globale. Cela signifie que la variable globale est accessible à l'intérieur ou à l'extérieur de la fonction.

```python
x = "global"

def foo():
    print("x inside :", x)

foo()
print("x outside:", x)
```

Lorsque nous exécutons le code, le résultat sera:

```
x inside : global
x outside: global
```

Dans le code ci-dessus, nous avons créé `x` comme variable globale et défini un `foo()` pour imprimer la variable globale `x` . Enfin, nous appelons le `foo()` qui affichera la valeur de `x` .

Que faire si vous voulez changer la valeur de `x` dans une fonction?

```python
x = "global"

def foo():
    x = x * 2
    print(x)
foo()
```

Lorsque nous exécutons le code, le résultat sera:

```
UnboundLocalError: local variable 'x' referenced before assignment
```

La sortie affiche une erreur car Python traite `x` comme une variable locale et `x` n'est pas non plus défini à l'intérieur `foo()`.

## Variables locales

Une variable déclarée dans le corps de la fonction ou dans la portée locale est appelée variable locale.

```python
def foo():
    y = "local"

foo()
print(y)
```

Lorsque nous exécutons le code, le résultat sera:

```
NameError: name 'y' is not defined
```

La sortie affiche une erreur, car nous essayons d'accéder à une variable locale y dans une portée globale alors que la variable locale ne fonctionne que dans  `foo()` une portée locale ou locale.

Voyons un exemple de la façon dont une variable locale est créée en Python.

```python
def foo():
    y = "local"
    print(y)

foo()
```

Lorsque nous exécutons le code, il affichera:

```
local
```

Jetons un coup d'oeil au problème précédent où `x` était une variable globale et nous voulions modifier `x` à l'intérieur `foo()`.

## Variables globales et locales

Ici, nous allons montrer comment utiliser les variables globales et les variables locales dans le même code.

```python
x = "global"

def foo():
    global x
    y = "local"
    x = x * 2
    print(x)
    print(y)

foo()
```

Lorsque nous exécutons le code, le résultat sera:

```
global global
local
```

Dans le code ci-dessus, nous déclarons `x` comme global et `y` comme variable locale dans le `foo()`. Ensuite, nous utilisons l'opérateur de multiplication `*` pour modifier la variable globale `x`  et nous imprimons à la fois `x` et `y` .

Après avoir appelé le foo(), la valeur de x devient global globalparce que nous avons utilisé le x * 2pour imprimer deux fois global. Après cela, nous imprimons la valeur de la variable locale y c'est-à-dire local.

```python
x = 5

def foo():
    x = 10
    print("local x:", x)

foo()
print("global x:", x)
```

Lorsque nous exécutons le code, le résultat sera:

```
local x: 10
global x: 5
```

Dans le code ci-dessus, nous avons utilisé le même nom `x` pour les variables globales et locales. Nous obtenons des résultats différents lorsque nous imprimons la même variable, car la variable est déclarée dans les deux portées, c’est-à-dire la portée locale à l’intérieur `foo()` et la portée globale à l’extérieur foo().

Lorsque nous imprimons la variable à l'intérieur de `foo()` la sortie local x: 10, cela s'appelle la portée locale de la variable.

De même, lorsque nous imprimons la variable en dehors de la `foo()`, elle sort `global x: 5`, cela s'appelle la portée globale de la variable.

## Variables non locales

Les variables non locales sont utilisées dans les fonctions imbriquées dont l'étendue locale n'est pas définie. Cela signifie que la variable ne peut être ni dans la portée locale ni dans la portée globale.

Voyons un exemple sur la façon dont une variable globale est créée en Python.

Nous utilisons des nonlocalmots clés pour créer une variable non locale.

```python
def outer():
    x = "local"

    def inner():
        nonlocal x
        x = "nonlocal"
        print("inner:", x)

    inner()
    print("outer:", x)

outer()
```

Lorsque nous exécutons le code, le résultat sera:

```
intérieur: non local
externe: non local
```

Dans le code ci-dessus, il y a une fonction imbriquée `inner()`. Nous utilisons des nonlocalmots clés pour créer une variable non locale. La fonction `inner()` est définie dans le cadre d'une autre fonction `outer()`.

Remarque: Si nous modifions la valeur de la variable non locale, les modifications apparaissent dans la variable locale.

## Modules

Les modules font référence à un fichier contenant des instructions et des définitions Python.

Un fichier contenant du code Python, par exemple:, `example.py` est appelé un module et son nom de module serait example.

Nous utilisons des modules pour décomposer les grands programmes en petits fichiers faciles à gérer et organisés. De plus, les modules permettent de réutiliser le code.

Nous pouvons définir nos fonctions les plus utilisées dans un module et les importer, plutôt que de copier leurs définitions dans différents programmes.

Laissez-nous créer un module. Tapez ce qui suit et enregistrez-le sous `example.py`.

```python
# Python Module example

def add(a, b):
   """This program adds two
   numbers and return the result"""

   result = a + b
   return result
```

## Importer des modules

Nous pouvons importer les définitions dans un module vers un autre module ou l’interpréteur interactif en Python.

Nous utilisons le mot-clé `import` pour le faire. Pour importer notre module précédemment défini, tapez `example` ce qui suit dans l'invite Python.

```python
import example
```

En utilisant le nom du module, nous pouvons accéder à la fonction en utilisant l'opération point `.`. Par exemple:

```python
example.add(4,5.5)
9.5
```

## Importer avec renommer

Nous pouvons importer un module en le renommant comme suit.

```python
# import module by renaming it

import math as m
print("The value of pi is", m.pi)
```

Nous avons renommé le module `math` en tant que `m`. Cela peut nous faire économiser du temps de frappe dans certains cas.

Notez que le nom `math` n'est pas reconnu dans notre champ d'application. Par conséquent, `math.pi` est invalide, `m.pi` est la mise en œuvre correcte.

## Python from ... instruction import

Nous pouvons importer des noms spécifiques d'un module sans importer le module dans son ensemble. Voici un exemple.

```python
# import only pi from math module

from math import pi
print("The value of pi is", pi)
```

Nous avons importé uniquement l'attribut pi du module.

## Importer tous les noms

Nous pouvons importer tous les noms (définitions) d'un module en utilisant la construction suivante.

```python
# import all names from the standard module math

from math import *
print("The value of pi is", pi)
```

## Package

Nous ne stockons généralement pas tous nos fichiers sur notre ordinateur au même endroit. Nous utilisons une hiérarchie de répertoires bien organisée pour un accès plus facile.

Les fichiers similaires sont conservés dans le même répertoire, par exemple, nous pouvons conserver toutes les chansons dans le répertoire "music". De manière analogue, Python a des packages pour les répertoires et les modules pour les fichiers.

La taille de notre programme d'application augmentant avec de nombreux modules, nous plaçons des modules similaires dans un seul paquet et des modules différents dans des packages différents. Cela rend un projet (programme) facile à gérer et conceptuellement clair.

De même, un répertoire pouvant contenir des sous-répertoires et des fichiers, un package Python peut avoir des sous-packages et des modules.

Un répertoire doit contenir un fichier nommé `__init__.py` afin que Python le considère comme un package. Ce fichier peut être laissé vide mais nous plaçons généralement le code d'initialisation pour ce package dans ce fichier.

Voici un exemple. Supposons que nous développions un jeu, une organisation possible des packages et des modules pourrait être comme indiqué dans la figure ci-dessous.


## Importation d'un module à partir d'un package

Nous pouvons importer des modules à partir de packages en utilisant l'opérateur point (.).

Par exemple, si vous souhaitez importer le startmodule dans l'exemple ci-dessus, procédez comme suit.

```python
import Game.Level.start
```

Maintenant, si ce module contient une fonction nommée `select_difficulty()`, nous devons utiliser le nom complet pour le référencer.

```python
Game.Level.start.select_difficulty(2)
```

Si cette construction semble longue, nous pouvons importer le module sans le préfixe de package comme suit.

```python
from Game.Level import start
```

Nous pouvons maintenant appeler la fonction simplement comme suit.

```python
start.select_difficulty(2)
```

Une autre façon d'importer uniquement la fonction requise (ou la classe ou la variable) d'un module dans un package serait la suivante.

```python
from Game.Level.start import select_difficulty
```

Maintenant, nous pouvons appeler directement cette fonction.

```python
select_difficulty(2)
```

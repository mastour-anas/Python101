# Python Itérateurs

Les itérateurs sont partout en Python. Ils sont élégamment mis en œuvre dans les boucles `for`, les compréhensions, les générateurs, etc. mais cachés à la vue.

Iterator en Python est simplement un objet sur lequel on peut itérer. Un objet qui renverra des données, un élément à la fois.

Techniquement parlant, Python objet iterator doit implémenter deux méthodes spéciales,`__iter__()` et `__next__()`, collectivement appelés le protocole itérateur .

Un objet est appelé itérable si on peut en obtenir un itérateur. La plupart des conteneurs intégrés en Python tels que: `list` , `tuple` , `string`, etc. sont itérables.

La fonction `iter()` (qui à son tour appelle la méthode `_iter__()`) renvoie un itérateur.

## Itérer à travers un itérateur en Python

Nous utilisons la fonction `next()` pour parcourir manuellement tous les éléments d'un itérateur. Lorsque nous atteindrons la fin et qu'il n'y aura plus de données à retourner, cela augmentera `StopIteration`. Voici un exemple.

```python
# define a list
my_list = [4, 7, 0, 3]

# get an iterator using iter()
my_iter = iter(my_list)

## iterate through it using next() 

#prints 4
print(next(my_iter))

#prints 7
print(next(my_iter))

## next(obj) is same as obj.__next__()

#prints 0
print(my_iter.__next__())

#prints 3
print(my_iter.__next__())

## This will raise error, no items left
next(my_iter)
```

Une manière plus élégante d'itérer automatiquement est d'utiliser la boucle `for` . En utilisant cela, nous pouvons parcourir tous les objets pouvant renvoyer un itérateur, par exemple la liste, la chaîne, le fichier, etc.

```python
for element in my_list:
    print(element)
```

## Comment fonctionne la boucle?

Comme nous le voyons dans l'exemple ci-dessus, la boucle `for` a pu effectuer une itération automatique dans la liste.

En fait, la boucle `for` peut itérer sur toute itération. Regardons de plus près comment la boucle `for` est réellement implémentée en Python.

```python
for element in iterable:
    # do something with element
```

Est réellement implémenté comme.

```python
# create an iterator object from that iterable
iter_obj = iter(iterable)

# infinite loop
while True:
    try:
        # get the next item
        element = next(iter_obj)
        # do something with element
    except StopIteration:
        # if StopIteration is raised, break from loop
        break
```

Donc, en interne, la boucle `for` crée un objet itérateur, `iter_obj` en appelant `iter()` l'itérable.

Ironiquement, cette boucle `for est en réalité une boucle while infinie .

Dans la boucle, il appelle `next()` pour obtenir l'élément suivant et exécute le corps de la boucle `for` avec cette valeur. Après que tous les éléments d'échappement, `StopIteration` est soulevé et la boucle `for` se termine.

## Construire votre propre itérateur en Python

Construire un itérateur à partir de zéro est facile en Python. Nous devons juste mettre en œuvre les méthodes `__iter__()` et `__next__()`.

La méthode `__iter__()` retourne l'objet itérateur lui-même. Si nécessaire, une initialisation peut être effectuée.

La méthode `__next__()` doit renvoyer l'élément suivant dans la séquence. En atteignant la fin, et dans les appels ultérieurs, il doit augmenter StopIteration.

Ici, nous montrons un exemple qui nous donnera la prochaine puissance de 2 dans chaque itération. L'exposant de puissance commence à zéro jusqu'à un nombre défini par l'utilisateur.

```python
class PowTwo:
    """Class to implement an iterator
    of powers of two"""

    def __init__(self, max = 0):
        self.max = max

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n <= self.max:
            result = 2 ** self.n
            self.n += 1
            return result
        else:
            raise StopIteration
```

Maintenant, nous pouvons créer un itérateur et le parcourir comme suit.

```python
>>> a = PowTwo(4)
>>> i = iter(a)
>>> next(i)
1
>>> next(i)
2
>>> next(i)
4
>>> next(i)
8
>>> next(i)
16
>>> next(i)
Traceback (most recent call last):
...
StopIteration
```

Nous pouvons également utiliser une forboucle pour parcourir notre classe d'itérateurs.

```python
>>> for i in PowTwo(5):
...     print(i)
...
1
2
4
8
16
32
```

# Python Générateurs

Il y a beaucoup de surcharge dans la construction d'un itérateur en Python ; nous devons implémenter une classe méthode `__iter__()` et une `__next__()` , suivre les états internes, relancer `StopIteration` quand il n'y a pas de valeurs à retourner, etc.

C'est à la fois long et contre-intuitif. Générateur vient en secours dans de telles situations.

Les générateurs Python sont un moyen simple de créer des itérateurs. Tous les frais généraux mentionnés ci-dessus sont automatiquement gérés par les générateurs de Python.

En termes simples, un générateur est une fonction qui renvoie un objet (itérateur) sur lequel nous pouvons effectuer une itération (une valeur à la fois).

## Comment créer un générateur en Python?

Il est assez simple de créer un générateur en Python. C'est aussi simple que de définir une fonction normale avec une instruction `yield` plutôt qu'une instruction `return`.

Si une fonction contient au moins une déclaration `yield` (il peut contenir d' autres `yield` ou `return`), il devient une fonction de générateur. Les deux `yield` et `return` retournera une valeur d'une fonction.

La différence est que, si `return` termine une fonction entièrement, la `yield` interrompt la fonction en sauvegardant tous ses états et continue ensuite à partir de là lors des appels successifs.

## Différences entre la fonction Générateur et une fonction normale

Voici comment une fonction de générateur diffère d'une fonction normale .

- La fonction générateur contient une ou plusieurs yieldinstructions.
- Lorsqu'il est appelé, il retourne un objet (itérateur) mais ne lance pas l'exécution immédiatement.
- Les méthodes comme `__iter__()` et `__next__()` sont implémentées automatiquement. Nous pouvons donc parcourir les éléments en utilisant `next()`.
- Une fois que la fonction cède, la fonction est mise en pause et le contrôle est transféré à l'appelant.
- Les variables locales et leurs états sont mémorisés entre les appels successifs.
- Enfin, lorsque la fonction se termine, `StopIteration` est automatiquement déclenchée lors d'appels ultérieurs.

Voici un exemple pour illustrer tous les points énoncés ci-dessus. Nous avons une fonction de générateur nommée `my_gen()` avec plusieurs instructions `yield`.

```python
# A simple generator function
def my_gen():
    n = 1
    print('This is printed first')
    # Generator function contains yield statements
    yield n

    n += 1
    print('This is printed second')
    yield n

    n += 1
    print('This is printed at last')
    yield n
```

```shell
>>> # It returns an object but does not start execution immediately.
>>> a = my_gen()

>>> # We can iterate through the items using next().
>>> next(a)
This is printed first
1
>>> # Once the function yields, the function is paused and the control is transferred to the caller.

>>> # Local variables and theirs states are remembered between successive calls.
>>> next(a)
This is printed second
2

>>> next(a)
This is printed at last
3

>>> # Finally, when the function terminates, StopIteration is raised automatically on further calls.
>>> next(a)
Traceback (most recent call last):
...
StopIteration
>>> next(a)
Traceback (most recent call last):
...
StopIteration
```

Une chose intéressante à noter dans l'exemple ci-dessus est que la valeur de la variable `n` est mémorisée entre chaque appel.

Contrairement aux fonctions normales, les variables locales ne sont pas détruites lorsque la fonction cède. De plus, l'objet générateur ne peut être itéré qu'une seule fois.

Pour relancer le processus, nous devons créer un autre objet générateur en utilisant quelque chose comme `a = my_gen()`.

Note: Une dernière chose à noter est que nous pouvons utiliser des générateurs avec des boucles for directement.

En effet, une forboucle prend un itérateur et l'itère en utilisant la fonction `next()`. Il se termine automatiquement quand `StopIteration` est déclenché.

```python
def rev_str(my_str):
    length = len(my_str)
    for i in range(length - 1,-1,-1):
        yield my_str[i]

# For loop to reverse the string
# Output:
# o
# l
# l
# e
# h
for char in rev_str("hello"):
     print(char)
```

When you run the program, the output will be:

```
This is printed first
1
This is printed second
2
This is printed at last
3
```

## Expression du générateur Python

Des générateurs simples peuvent être facilement créés à la volée en utilisant des expressions de générateur. Cela facilite la construction de générateurs.

Comme la fonction lambda crée une fonction anonyme , l'expression du générateur crée une fonction de générateur anonyme.

La syntaxe de l'expression du générateur est similaire à celle d'une compréhension de liste en Python . Mais les crochets sont remplacés par des parenthèses rondes.

La principale différence entre une compréhension de liste et une expression de générateur est que, tandis que la compréhension de liste produit la liste entière, l’expression de générateur produit un élément à la fois.

Ils sont un peu paresseux, produisant des articles seulement quand on leur demande. Pour cette raison, une expression de générateur est beaucoup plus efficace en mémoire qu'une compréhension de liste équivalente.

```python
# Initialize the list
my_list = [1, 3, 6, 10]

# square each term using list comprehension
# Output: [1, 9, 36, 100]
[x**2 for x in my_list]

# same thing can be done using generator expression
# Output: <generator object <genexpr> at 0x0000000002EBDAF8>
(x**2 for x in my_list)
```

Nous pouvons voir ci-dessus que l'expression du générateur n'a pas produit le résultat requis immédiatement. Au lieu de cela, il a renvoyé un objet générateur avec des éléments de production à la demande.

```python
# Intialize the list
my_list = [1, 3, 6, 10]

a = (x**2 for x in my_list)
# Output: 1
print(next(a))

# Output: 9
print(next(a))

# Output: 36
print(next(a))

# Output: 100
print(next(a))

# Output: StopIteration
next(a)
```

L'expression de générateur peut être utilisée à l'intérieur de fonctions. Lorsqu'elles sont utilisées de cette manière, les parenthèses rondes peuvent être supprimées.

```
>>> sum(x**2 for x in my_list)
146

>>> max(x**2 for x in my_list)
100
```

## Pourquoi les générateurs sont-ils utilisés en Python?

Pourquoi les générateurs sont-ils utilisés en Python?

### Facile à mettre en œuvre

Les générateurs peuvent être implémentés de manière claire et concise par rapport à leur homologue de classe d'itérateurs. Voici un exemple pour implémenter une séquence de puissance de 2 utilisant une classe d'itérateur.

```python
class PowTwo:
    def __init__(self, max = 0):
        self.max = max

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n > self.max:
            raise StopIteration

        result = 2 ** self.n
        self.n += 1
        return result
```

C'était long. Maintenant, faisons de même en utilisant une fonction de générateur.

```python
def PowTwoGen(max = 0):
    n = 0
    while n < max:
        yield 2 ** n
        n += 1
```

Depuis, les générateurs conservent automatiquement les détails, ils étaient concis et beaucoup plus propres.

### Mémoire efficace

Une fonction normale pour renvoyer une séquence créera toute la séquence en mémoire avant de renvoyer le résultat. Ceci est un excès si le nombre d'éléments dans la séquence est très grand.

L'implémentation du générateur de cette séquence est compatible avec la mémoire et est préférée car elle ne produit qu'un élément à la fois.

### Représenter un flux infini

Les générateurs sont un excellent moyen de représenter un flux de données infini. Les flux infinis ne peuvent pas être stockés en mémoire et comme les générateurs ne produisent qu'un élément à la fois, ils peuvent représenter un flux infini de données.

L'exemple suivant peut générer tous les nombres pairs (au moins en théorie).

```python
def all_even():
    n = 0
    while True:
        yield n
        n += 2
```

### Générateurs de pipeline

Les générateurs peuvent être utilisés pour canaliser une série d'opérations. Ceci est mieux illustré en utilisant un exemple.

Supposons que nous ayons un fichier journal d'une célèbre chaîne de restauration rapide. Le fichier journal a une colonne (4ème colonne) qui garde la trace du nombre de pizzas vendues toutes les heures et nous voulons le résumer pour trouver le nombre total de pizzas vendues en 5 ans.

Supposons que tout est en chaîne et que les nombres non disponibles sont marqués «N / A». Une mise en œuvre de générateur de ce pourrait être comme suit.

```python
with open('sells.log') as file:
    pizza_col = (line[3] for line in file)
    per_hour = (int(x) for x in pizza_col if x != 'N/A')
    print("Total pizzas sold = ",sum(per_hour))
```

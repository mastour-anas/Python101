# Conventions de syntaxe en python

les conventions de syntaxe en Python sont proposé dans `PEP` ou `Python enhancement Proposal` un document de conception fournissant des informations a la communauté Python ou décrivant une nouvelle fonctionalité pour Python, ses processus ou son environement.

> "A universal convention supplies all of maintainability, clarity, consistency, and a foundation for good programming habits too. What it doesn't do is insist that you follow it against your will. That's Python!" `Tim Peters on comp.lang.python, 2001-06-16`

- les noms de variables commencent par une miniscule et les parties sont accollées avec une majuscule:

```python
foo, fooBar, mixedCase
```

- les variable privees sont précédés de deux soulignés:

```python
__FooBar = 2
```

- les noms de classe utilisent les mêmes conventions que les variables, mais commencent par une majuscule:

```python
class Span, class SpanAndEgg
```

- les noms de méthodes sont en miniscules et les parties sont separées par un souligné (`snake_case`):

```python
def foo_bar(self):
```

- les méthodes privées sont précédées de deux soulignes:

```python
def __foo_bar(self):
```

- la maniere la plus simple de nommer un callback est de terminer son nom par la notation conventionnelle `_cb`:

```python
def event_cb(self, widget, event, *args):
```

- les noms de constantes sont écrits en majuscules, avec un souligné comme separateur:

```python
FOO_BAR = 3
```

- les packages et les modules doivent être réduites et en miniscule, l'ajout les soulignés pour la lisibilité (déconseiller).

## Mots-cle reserves

Ces mots ont une signification dans le langage et ne peuvent être utilisés pour des noms de variables, de fonctions, etc...

`and` ,`as` ,`assert` ,`break` ,`class` ,`continue` ,`def` ,`del` ,`elif` ,`else` ,`except` ,`exec` ,`False` ,`finally` ,`for` ,`from` ,`global` ,`if` ,`import` ,`in` ,`is` ,`lambda` ,`None` ,`nonlocal` ,`not` ,`or` ,`pass` ,`print` ,`raise` ,`return` ,`True` ,`try` ,`while` ,`with` ,`yield`.

## Recommendations

- Le code doit être écrit dans une façon qui sera compatible avec les autres interpréteurs de python (PyPy, Jython, IronPython, Cython, Psyco...).

    Par exemple, CPython n'implémente pas effiencement la concatenation des chaine de caractaire dans cette syntax `a += b` ou `a = a + b`, cette optimisation est fragile dans CPython (marche que dans quelque interpréteurs), pour plus perfomance, il est imperative d'utiliser `''.join()`, elle assure la concataination des 2 chaines de caracteres dans plusieur interpréteurs.

- Pour la comparaison avec un `singelons` comme `None`, utiliser `is` ou `is not` et pas `=`.
- Fait attention quand vous écrivez `if x` par exemple `if x is not None`, lors du test si une variable ou un argument par défaut à `None` a été défini sur une autre valeur. L'autre valeur peut avoir un type (tel qu'un conteneur) qui pourrait être faux dans un contexte booléen!

```python
>>> t =[]
>>> print(t is None)
False
>>>
```

# datetime

Le module `datetime` fournit un certain nombre de types pour traiter les dates, les heures et les intervalles de temps. Ce module remplace les mécanismes de temps basés sur les entiers / tuple dans le module temporel par une interface plus orientée objet.

Le module contient les types suivants:

- Le type datetime représente une date et une heure au cours de cette journée.
- Le type de date représente juste une date, entre l'année 1 et 9999 (voir ci-dessous pour plus d'informations sur le calendrier utilisé par le module datetime)
- Le type d' heure représente une heure, indépendante de la date.
- Le type timedelta représente la différence entre deux objets d'heure ou de date.
- Le type tzinfo est utilisé pour implémenter la prise en charge du fuseau horaire pour les objets time et datetime;

## datetime

Les objets du type datetime représentent une date et une heure dans un fuseau horaire. Sauf indication contraire, les objets datetime utilisent le terme «temps naïf», ce qui signifie que l'application doit garder une trace de son fuseau horaire. Le module datetime fournit un support pour les fuseaux horaires; 

```python
>>> from datetime import datetime
>>> datetime(2000, 1, 1)
datetime.datetime(2000, 1, 1, 0, 0)
```

Pour créer un objet datetime pour une date donnée, appelez le constructeur datetime :

`datetime(année, mois, jour, heure, minute, seconde, microseconde, fuseau horaire)`

```python
>>> maintenant = datetime.now()
>>> maintenant
datetime.datetime(2018, 12, 24, 18, 20, 4, 534918)
>>> maintenant.year
2018
>>> maintenant.month
12
>>> maintenant.day
24
>>> maintenant.hour
18
>>> maintenant.minute
20
>>> maintenant.second
4
>>> maintenant.microsecond
534918
>>> maintenant.isocalendar() # année, semaine, jour
(2018, 52, 2)
```

Vous pouvez également créer des objets datetime en utilisant l'une des nombreuses fonctions de fabrique intégrées (mais notez que toutes ces fonctions sont fournies en tant que méthodes de classe et non en tant que fonctions de module):

```python
import datetime
import time

print datetime.datetime(2003, 8, 4, 21, 41, 43)

print datetime.datetime.today()
print datetime.datetime.now()
print datetime.datetime.fromtimestamp(time.time())

print datetime.datetime.utcnow()
print datetime.datetime.utcfromtimestamp(time.time())

2003-08-04 21:41:43
2003-08-04 21:41:43.522000
2003-08-04 21:41:43.522000
2003-08-04 21:41:43.522000
2003-08-04 19:41:43.532000
2003-08-04 19:41:43.532000
```

Le type datetime fournit également d'autres méthodes de formatage, y compris la méthode strftime très générale:

```python
import datetime

d = datetime.date(2003, 7, 29)

print d
print d.year, d.month, d.day

print datetime.date.today()

2003-07-29
2003 7 29
2003-08-07
```

## les types de date et d'heure
Le type de date représente les portions de date d'un objet datetime.

```python
import datetime

d = datetime.date(2003, 7, 29)

print d
print d.year, d.month, d.day

print datetime.date.today()
2003-07-29
2003 7 29
2003-08-07
```

Le type de temps est similaire; il représente la partie temps, avec une résolution en microseconde.

```python
import datetime

t = datetime.time(18, 54, 32)

print t
print t.hour, t.minute, t.second, t.microsecond
18:54:32
18 54 32 0
```

Le type datetime fournit une méthode pour extraire des objets date et heure , ainsi qu'une méthode de classe qui combine deux objets en un seul objet datetime :

```python
import datetime

now = datetime.datetime.now()

d = now.date()
t = now.time()

print now
print d, t
print datetime.datetime.combine(d, t)
2003-08-07 23:19:57.926000
2003-08-07 23:19:57.926000
2003-08-07 23:19:57.926000
```

## calcule les différences entre les temps:

Le module timedelta est très utile pour calculer les différences entre les temps:

```python
from datetime import datetime, timedelta
now = datetime.now()
then = datetime(2016, 5, 23) # datetime.datetime(2016, 05, 23, 0, 0, 0)
delta = now-then
print(delta.days)
# 60
print(delta.seconds)
# 40826
```

`delta` est de type timedelta

Pour obtenir n jours après et n jours avant la date, nous pourrions utiliser:

```python
def get_n_days_after_date(date_format="%d %B %Y", add_days=120):
    date_n_days_after = datetime.datetime.now() + timedelta(days=add_days)
    return date_n_days_after.strftime(date_format)

def get_n_days_before_date(self, date_format="%d %B %Y", days_before=120):
    date_n_days_ago = datetime.datetime.now() - timedelta(days=days_before)
    return date_n_days_ago.strftime(date_format)
```

## Switching entre les fuseaux horaires

Pour passer d'un fuseau horaire à un autre, vous avez besoin d'objets datetime compatibles avec les fuseaux horaires.

```python

from datetime import datetime
from dateutil import tz
utc = tz.tzutc()
local = tz.tzlocal()
utc_now = datetime.utcnow()
utc_now # Not timezone-aware.
utc_now = utc_now.replace(tzinfo=utc)
utc_now # Timezone-aware.
local_now = utc_now.astimezone(local)
local_now # Converted to local time.
```

## les opération arithmetique date 

Les dates n'existent pas isolément. Il est courant que vous ayez besoin de trouver la durée entre les dates ou
déterminer quelle sera la date demain. Ceci peut être accompli en utilisant des objets timedelta

```python
import datetime
today = datetime.date.today()
print('Today:', today)
yesterday = today - datetime.timedelta(days=1)
print('Yesterday:', yesterday)
tomorrow = today + datetime.timedelta(days=1)
print('Tomorrow:', tomorrow)
print('Time between tomorrow and yesterday:', tomorrow - yesterday)
Today: 2016-04-15
Yesterday: 2016-04-14
Tomorrow: 2016-04-16
Difference between tomorrow and yesterday: 2 days, 0:00:00
```


# Python os

Les modules `os` et `sys` fournissent de nombreux outils pour gérer les noms de fichiers, les chemins d'accès et les répertoires. Le module os contient deux sous-modules `os.sys` (identiques à `sys` ) et `os.path` qui sont dédiés au système et aux répertoires; respectivement.

Dans la mesure du possible, vous devez utiliser les fonctions fournies par ces modules pour les manipulations de fichiers, de répertoires et de chemins. Ces modules sont des enveloppes pour les modules spécifiques à la plate-forme. Les fonctions telles que `os.path.split` fonctionnent sous `UNIX`, `Windows`, `Mac OS` et toute autre plate-forme prise en charge par `Python`.

Vous pouvez créer un chemin multi-plateforme en utilisant le symbole de séparation approprié:

```python
>>> import os
>>> import os.path
>>> os.path.join(os.sep, 'home', 'user', 'work')
'/home/user/work'

>>> os.path.split('/usr/bin/python')
('/usr/bin', 'python')
```

Le module os a beaucoup de fonctions. Nous ne les couvrirons pas complètement, mais cela pourrait être un bon début pour utiliser le module.

## Manipulation des répertoires  

La fonction `getcwd()` renvoie le répertoire en cours (au format unicode avec `getcwdu()`).

Le répertoire actuel peut être modifié en utilisant `chdir()`:

```python
os.chdir(path)
```

La fonction `listdir()` renvoie le contenu d'un répertoire. Notez, cependant, qu'il mélange les répertoires et les fichiers.

La fonction `mkdir()` crée un répertoire. Il renvoie une erreur si le répertoire parent n'existe pas. Si vous voulez également créer le répertoire parent, vous devriez plutôt utiliser `makedirs()`:

```Python
>>> os.mkdir('temp') # creates temp directory inside the current directory
>>> os.makedirs("/tmp/temp/temp")
```

Une fois créé, vous pouvez supprimer un répertoire vide avec `rmdir()`:

```python
>>> import  os
>>> os.mkdir('/tmp/temp')
>>> os.rmdir('/tmp/temp')
```

Vous pouvez supprimer tous les répertoires d'un répertoire (s'il n'y en a pas) en utilisant `os.removedirs()` .

Si vous souhaitez supprimer un répertoire non vide, utilisez `shutil.rmtree()` (avec précaution).

## Supprimer un fichier

Pour supprimer un fichier, utilisez `os.remove()`. Il déclenche l'exception `OSError` si le fichier ne peut pas être supprimé. Sous Linux, vous pouvez également utiliser `os.unlink()`.

## Renommer des fichiers ou des répertoires

Vous pouvez renommer un fichier d'un ancien nom en un nouveau en utilisant `os.rename()`. Voir aussi `os.renames()`.

## Permission

vous pouvez changer le mode d'un fichier en utilisant `chmod()`. Voir aussi `chown`, `chroot`, `fchmod`, `fchown`.

Le `os.access()` vérifie l'autorisation d'accès spécifié dans l'argument de mode. Retourne 1 si l'accès est accordé, 0 sinon. Le mode peut être:

|<span>|                                 |
|------|-----------------------------------|
|os.F_OK|Valeur à transmettre en tant que paramètre de mode d'accès () pour tester l'existence du chemin.|
|os.R_OK:|Valeur à inclure dans le paramètre mode de access () pour tester la lisibilité du chemin.|
|os.W_OK|Valeur à inclure dans le paramètre mode de access () pour tester la possibilité d'écriture du chemin.|
|os.X_OK|Valeur à inclure dans le paramètre mode de access () pour déterminer si le chemin peut être|

```python
>>> os.access("validFile", os.F_OK)
True
```

## Les attributs de la plate-forme croisée

Un autre caractère utilisé par le système d'exploitation pour séparer les composants pathame est fourni par os.altsep() .

Le fichier os.curdir() fait référence au répertoire en cours. . pour unix et windows et : pour Mac OS.

Une autre fonction multi-plateforme qui pourrait être utile est le séparateur de ligne. En effet, le caractère final qui termine une ligne est codé différemment sous Linux, Windows et MAC. Par exemple sous Linux, c'est le caractère n mais vous pouvez avoir r ou rn. L'utilisation de `os.linese()` garantit l'utilisation d'un caractère universel line_ending.

Le `os.uname` donne plus d'informations sur votre système:

```Python
>>> os.uname()
('Linux', 'amastour-vm', '4.15.0-34-generic', '#37~16.04.1-Ubuntu SMP Tue Aug 28 10:44:06 UTC 2018', 'x86_64')
```

La fonction `os.name()` renvoie le module dépendant du système d'exploitation (par exemple, posix, doc, mac, ...)

La fonction `os.pardir()` fait référence au répertoire parent (.. pour unix et windows et :: pour Mac OS).

La fonction `os.pathsep()` (également présente dans os.path.sep() ) renvoie le séparateur de chemin correct pour votre système (barre oblique / sous Linux et barre oblique inverse sous Windows).

Enfin, `os.sep()` est le caractère qui sépare les composants de chemin d'accès (/ pour Unix, pour Windows et: pour Mac OS). Il est également disponible dans `os.path.sep()`


http://thomas-cokelaer.info/tutorials/python/module_os.html

```python
>>> # under linux
>>> os.path.sep
'/'
```

## En savoir plus sur les répertoires et les fichiers 

os.path fournit des méthodes pour extraire des informations sur les noms de chemin et de fichier:

```python
>>> os.path.curdir # returns the current directory ('.')
>>> os.path.isdir(dir) # returns True if dir exists
>>> os.path.isfile(file) # returns True if file exists
>>> os.path.islink(link) # returns True if link exists
>>> os.path.exists(dir) # returns True if dir exists (full pathname or filename)
>>> os.path.getsize(filename) # returns size of a file without opening it.
```

Vous pouvez accéder à l'heure de la dernière modification d'un fichier. Néanmoins, la sortie n'est pas conviviale pour l'utilisateur. Sous Unix, il correspond à l'heure depuis le 1er janvier 1970 (GMT) et sous Mac OS depuis le 1er janvier 1904 (GMT) Utilisez le module de temps pour faciliter la lecture:

```python
>>> import time
>>> mtime = os.path.getmtime(filename) # returns time when the file was last modified
>>> print time.ctime(mtime)
Tue Jan 01 02:02:02 2000
```

os.path.getctime() l'heure de modification des métadonnées d'un fichier.

Enfin, vous pouvez obtenir un ensemble d'informations à l'aide de os.stat() , comme la taille du fichier, l'heure d'accès, etc. La stat() renvoie un tuple de nombres qui vous donne des informations sur un fichier (ou un répertoire).

```python
>>> import stat
>>> import time
>>> def dump(st):
...    mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime = st
...    print "- size:", size, "bytes"
...    print "- owner:", uid, gid
...    print "- created:", time.ctime(ctime)
...    print "- last accessed:", time.ctime(atime)
...    print "- last modified:", time.ctime(mtime)
...    print "- mode:", oct(mode)
...    print "- inode/dev:", ino, dev
>>> dump(os.stat("todo.txt"))
- size: 0 bytes
- owner: 1000 1000
- created: Wed Dec 19 19:40:02 2012
- last accessed: Wed Dec 19 19:40:02 2012
- last modified: Wed Dec 19 19:40:02 2012
- mode: 0100664
- inode/dev: 23855323 64770
```

### gerer les chemain

Pour obtenir le nom de base d'un chemin (dernier composant):

```python
>>> import os
>>> os.path.basename("/home/user/temp.txt")
temp.txt
```

Pour obtenir le nom de répertoire d'un chemin, utilisez `os.path.dirname()`:

```python
>>> import os
>>> os.path.dirname("/home/user/temp.txt")
/home/user
```

`os.path.abspath()` renvoie le chemin absolu d'un fichier:

```python
>>> import os
>>> os.path.abspath('temp.txt')
```

En résumé, considérez un fichier `temp.txt` dans `/home/user` :

|function|Output|
|--------|------|
|basename|‘temp.txt’|
|dirname|‘’|
|split|(‘’, ‘temp.txt’)|
|splitdrive|(‘’, ‘temp.txt’)|
|splitext|(‘temp’; ‘txt’)|
|abspath|‘/home/user/temp.txt|

La fonction `os.path.splitext()` sépare l'extension d'un fichier:

`os.path.walk()` scanne un répertoire de manière récursive et applique une fonction de chaque élément trouvé (voir aussi `os.walk()` ci-dessus):

```python
import os
for root, dirs, files in os.walk(".", topdown=False):
   for name in files:
      print(os.path.join(root, name))
   for name in dirs:
      print(os.path.join(root, name))
```


# Module Sys

Le module sys permet d'accéder aux fonctions et aux valeurs relatives à l'environnement d'exécution du programme, telles que les paramètres de la ligne de commande dans sys.argv ou la fonction sys.exit() pour terminer le processus en cours à partir de n'importe quel point
le flux de programme.
Bien que séparé dans un module, il est en fait intégré et, en tant que tel, sera toujours disponible dans des circonstances normales.

## sys.path

Le python path indique à python quels dossiers il doit prendre en compte pour sa recherche de modules. Vous pouvez voir cette liste de cette manière:

```python
>>> import sys
>>> sys.path
['/usr/lib/python2.7', '/usr/lib/python2.7/plat-x86_64-linux-gnu', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/usr/local/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/PILcompat', '/usr/lib/python2.7/dist-packages/gtk-2.0', '/usr/lib/python2.7/dist-packages/ubuntu-sso-client']
```

 Si vous souhaitez importer un module qui n’existe pas déjà en tant que module intégré dans la bibliothèque Python Standard, vous pouvez le faire en ajoutant le chemin d'accès au répertoire où se trouve votre module, sys.path

```python
import sys
sys.path.append("/path/to/directory/containing/your/module")
import mymodule
```

Il est important que vous ajoutiez le chemin d'accès au répertoire dans lequel se trouve mymodule, pas le chemin d'accès au module lui-même.

## sys.argv

sys.argv est la liste des arguments de ligne de commande transmis au programme Python. argv représente tous les éléments fournis via l'entrée en ligne de commande, il s'agit essentiellement d'un tableau contenant les arguments de ligne de commande de notre programme (test.py).

```python
import sys

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)
```

```shell
python test.py arg1 arg2 arg3
```

Cela produit le résultat suivant 

```python
Number of arguments: 4 arguments.
Argument List: ['test.py', 'arg1', 'arg2', 'arg3']
```

## argparse

Le module argparse facilite l'écriture d'interfaces de ligne de commande . Le programme définit les arguments requis et argparse déterminera comment les analyser sys.argv. Il génère également automatiquement des messages d'aide et d'utilisation et génère des erreurs lorsque les utilisateurs attribuent des arguments non valides au programme.

Commençons par un exemple très simple qui ne fait (quasiment) rien :


```python
import argparse
parser = argparse.ArgumentParser()
parser.parse_args()
```

Ce qui suit est le résultat de l’exécution du code :

```python
python3 prog.py
python3 prog.py --help
usage: prog.py [-h]

optional arguments:
  -h, --help  show this help message and exit
python3 prog.py --verbose
usage: prog.py [-h]
prog.py: error: unrecognized arguments: --verbose
python3 prog.py foo
usage: prog.py [-h]
prog.py: error: unrecognized arguments: foo
```

###  Créer un parseur

La première étape de l'utilisation de l' argparseoutil consiste à créer un objet ArgumentParser:

```python
parser = argparse.ArgumentParser(description='Short sample app')
```

L'objet ArgumentParser contiendra toutes les informations nécessaires pour analyser la ligne de commande en types de données Python.

### Ajouter des arguments

Remplir un ArgumentParser avec des informations sur les arguments du programme se fait en faisant des appels à la méthode add_argument(). Généralement, ces appels indiquent ArgumentParser comment utiliser les chaînes sur la ligne de commande et les transformer en objets. Cette information est stockée et utilisée lorsque parse_args() est appelée. Par exemple:

```python
parser.add_argument('-a', action="store_true", default=False)
parser.add_argument('-b', action="store", dest="b")
parser.add_argument('-c', action="store", dest="c", type=int)
```

Plus tard, l'appel parse_args() renverra un objet avec trois attributs, un boolean. L'attribut string , et l'attribut int.

```python
import argparse

parser = argparse.ArgumentParser(description='Short sample app')

parser.add_argument('-a', action="store_true", default=False)
parser.add_argument('-b', action="store", dest="b")
parser.add_argument('-c', action="store", dest="c", type=int)


args = parser.parse_args()
print(args)
```

### Argument Actions

Chacune des six actions intégrées peut être déclenchée lorsqu'un argument est rencontré.

store
Enregistrez la valeur après l'avoir éventuellement convertie dans un autre type. C'est l'action par défaut prise si aucune n'est explicitement spécifiée.
store_const
Enregistrez une valeur définie dans le cadre de la spécification d'argument, plutôt qu'une valeur provenant des arguments analysés. Ceci est généralement utilisé pour implémenter des indicateurs de ligne de commande qui ne sont pas des booléens.
store_true / store_false
Enregistrez la valeur booléenne appropriée. Ces actions sont utilisées pour implémenter des commutateurs booléens.
append
Enregistrez la valeur dans une liste. Plusieurs valeurs sont enregistrées si l'argument est répété.
append_const
Enregistrez une valeur définie dans la spécification de l'argument dans une liste.
version
Imprime les détails de la version du programme, puis quitte.


Cet exemple de programme illustre chaque type d'action, avec la configuration minimale requise pour que chacun fonctionne.

```python
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-s', action='store',
                    dest='simple_value',
                    help='Store a simple value')

parser.add_argument('-c', action='store_const',
                    dest='constant_value',
                    const='value-to-store',
                    help='Store a constant value')

parser.add_argument('-t', action='store_true',
                    default=False,
                    dest='boolean_t',
                    help='Set a switch to true')
parser.add_argument('-f', action='store_false',
                    default=True,
                    dest='boolean_f',
                    help='Set a switch to false')

parser.add_argument('-a', action='append',
                    dest='collection',
                    default=[],
                    help='Add repeated values to a list')

parser.add_argument('-A', action='append_const',
                    dest='const_collection',
                    const='value-1-to-append',
                    default=[],
                    help='Add different values to list')
parser.add_argument('-B', action='append_const',
                    dest='const_collection',
                    const='value-2-to-append',
                    help='Add different values to list')

parser.add_argument('--version', action='version',
                    version='%(prog)s 1.0')

results = parser.parse_args()
print('simple_value     = {}'.format(results.simple_value))
print('constant_value   = {}'.format(results.constant_value))
print('boolean_t        = {}'.format(results.boolean_t))
print('boolean_f        = {}'.format(results.boolean_f))
print('collection       = {}'.format(results.collection))
print('const_collection = {}'.format(results.const_collection))
```




# Expressions régulières

Les expressions régulières sont utilisées dans quasiment tous les langages. C'est un outil très puissant qui permet de vérifier si le contenu d'une variable a la forme de ce que l'on attend. Par exemple si on récupère un numéro de téléphone, on s'attend à ce que la variable soit composée de nombres et d'espaces (ou de tiret) mais rien de plus. Les expressions régulières permettent non seulement de vous avertir d'un caractère non désiré mais également de supprimer/modifier tous ceux qui ne sont pas désirables.

## Les bases

On utilise des symboles qui ont une signification:

`. ^ $ * + ? { } [ ] \ | ( )`

|<span>|                                                         |
|----|-----------------------------------------------------------|
|.   |Le point correspond à n'importe quel caractère.            |
|^     |Indique un commencement de segment mais signifie aussi "contraire de"|
|$     |Fin de segment|
|[xy]  |Une liste de segment possibble. Exemple [abc] équivaut à : a, b ou c|
|(x|y) |Indique un choix multiple type (ps|ump) équivaut à "ps" OU "UMP"|
|\d    |le segment est composé uniquement de chiffre, ce qui équivaut à [0-9].|
|\D    |le segment n'est pas composé de chiffre, ce qui équivaut à [^0-9].|
|\s    |Un espace, ce qui équivaut à [ \t\n\r\f\v].|
|\S    |Pas d'espace, ce qui équivaut à [^ \t\n\r\f\v].|
|\w    |Présence alphanumérique, ce qui équivaut à [a-zA-Z0-9_].|
|\W    |Pas de présence alphanumérique [^a-zA-Z0-9_].|
|\     |Est un caractère d'échappement|

Il est possible de d'imposer le nombre d'occurences avec la syntaxe suivante:

|<span>|                                                         |
|----|-----------------------------------------------------------|
|A{2}     | on attend à ce que la lettre A (en majuscule) se répète 2 fois consécutives.|
|BA{1,9}  | on attend à ce que le segment BA se répète de 1 à 9 fois consécutives.|
|BRA{,10} | on attend à ce que le segment BRA ne soit pas présent du tout ou présent jusqu'à 10 fois consécutives.|
|VO{1,}   | on attend à ce que le segment VO soit présent au mois une fois.|


|Symbole  |  Nb Caractères attendus |Exemple  |Cas possibles|
|---------|-------------------------|---------|-------------|
|?	|0 ou 1	|GR (.)? S	|GRS, GR O S, GR I S, GR A S|
|+	|1 ou plus	|GR (.)+ S	|GR O S, GR I S, GR A S|
|*	|0, 1 ou plus	|GR (.)* S	|GRS,GRO O ,GR III S,GR Olivier S|

On va abbrégé le plus rapidement possible tout cours théorique, la programmation c'est amusant quand c'est concret.

Prenons ce tutoriel comme un jeu: le but du jeu c'est d'anticiper si une expression est TRUE ou FALSE tout simplement

## La bibliothèque re

Lancez votre interpréteur python et importez la bibliothèque re .

```python
>>> import re
>>> print re.match(r"GR(.)?S", "GRIS")
<_sre.SRE_Match object at 0x7f37acd2c558>
```

Si la réponse n'est pas None c'est que le match correspond.

## Chercher une expression

Le match est très intéressant pour valider l'intégrité d'une variable, mais il est également possible de chercher des expressions spécifiques dans une chaine de caractères.

```python
>>> import re
>>> re.findall("([0-9]+)", "Hello 111 Goodby2 222")
['111', '222']
```

Il est également possible de chercher par groupe:

```python
>>> import re
>>> m = re.search(r"Bienvenue chez (?P<chezqui>\w+) ! Tu as (?P<age>\d+) ans ?", "Bienvenue chez olivier ! Tu as 32 ans")
>>> if m is not None:
...     print m.group('chezqui')
...     print m.group('age')
...
olivier
32
```

## Remplacer une expression

Pour remplacer une expression on utilise la méthode sub() .

```python
re.sub(r"t[0-9][0-9]", "foo", "my name t13 is t44 what t99 ever t44")
# Out: 'my name foo is foo what foo ever foo'
```

Le remplacement d'expression se fait sur tous les matchs possibles:

```python
>>> data = """
... olivier;engel;30ans;
... bruce;wayne;45ans;
... """
>>> print re.sub(r"(?P<prenom>\w+);(?P<nom>\w+);(?P<age>\w+);", r"\g<prenom>,\g<nom>,\g<age> ", data)

olivier,engel,30ans 
bruce,wayne,45ans
```

## Compiler une expression

Si vous êtes amenés à utiliser plusieurs fois la même expression (par exemple dans une boucle), vous pouvez la compiler pour gagner en performence

```python
import re
precompiled_pattern = re.compile(r"(\d+)")
matches = precompiled_pattern.search("The answer is 41!")
matches.group(1)
# Out: 41
matches = precompiled_pattern.search("Or was it 42?")
matches.group(1)
# Out: 42
```

http://apprendre-python.com/page-expressions-regulieres-regular-python